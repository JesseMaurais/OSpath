// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "err.hpp"
#include "env.hpp"
#include "cmd.hpp"
#include "opt.hpp"
#include "arg.hpp"
#include "usr.hpp"
#include "type.hpp"

namespace env::cmd
{
	bool iterator::operator!=(as_view<iterator> it) const
	{
		return buf != it.buf and ptr != it.ptr;
	}

	view iterator::operator*() const
	{
		return buf;
	}

	void iterator::operator++()
	{
		buf.clear();
		if (auto f = ptr.get())
		{
			while (not std::ferror(f))
			{
				auto c = std::fgetc(f);
				switch (c)
				{
				case EOF:
					ptr = nullptr;
				case '\n':
					return;
				}
				buf.push_back(c);
			}
			perror("ferror");
		}
	}

	range get(view line, size_t sz)
	{
		return
		{
			.begin = [=]
			{
				iterator it;
				it.ptr = open(line, "rx");
				if (it.ptr)
				{
					it.buf.reserve(sz);
					++it;
				}
				return it;
			},
			.end = []
			{
				return iterator();
			}
		};
	}

	range get(span args, size_t sz)
	{
		auto line = fmt::join(args);
		return get(line, sz);
	}

	range get(init args, size_t sz)
	{
		auto span = fwd::to_span(args);
		auto line = fmt::join(span);
		return get(line, sz);
	}

	range echo(view line)
	{
		return get("echo", line);
	}

	range list(view dir)
	{
		return get
		(
		#ifdef _WIN32
			"dir", "/b", dir
		#else
			"ls", dir
		#endif
		);
	}

	range find(view dir)
	{
		return get
		(
		#ifdef _WIN32
			"dir", "/b", "/s", dir
		#else
			"ls", "-R", dir
		#endif
		);
	}

	range read(view path)
	{
		return get
		(
		#ifdef _WIN32
			"type", path
		#else
			"cat", path
		#endif
		);
	}

	range which(view name)
	{
		return get
		(
		#ifdef _WIN32
			"where", name
		#else
			"which", "-a", name
		#endif
		);
	}

	range imports(view path)
	{
		return get
		(
		#ifdef _WIN32
			"dumpbin", "-nologo", "-imports", path
		#else
			"objdump", "-t", path
		#endif
		);
	}

	range exports(view path)
	{
		return get
		(
		#ifdef _WIN32
			"dumpbin", "-nologo", "-exports", path
		#else
			"objdump", "-T", path
		#endif
		);
	}

	range start(view path)
	{
		#ifdef _WIN32
		return get("start", "/d", path);
		#else
		{
			const pair test [] =
			{
				{ "xfce", "exo-open" },
				{ "gnome", "gnome-open" },
				{ "kde", "kde-open" },
				{ "", "xdg-open" },
			};

			for (auto [session, program] : test)
			{
				if (session.empty() or desktop(session))
				{
					for (auto use : which(program))
					{
						return get(use, path);
					}
				}
			}
			return { };
		}
		#endif
	}

	bool desktop(view name)
	{
		const auto current = env::usr::current_desktop();
		const auto left = to_lower(current);
		const auto right = to_lower(name);
		return left.find(right) != npos;
	}

	static vector pick()
	{
		constexpr auto zenity = "zenity", qarma = "qarma";

		if (desktop("KDE") or desktop("LXQT"))
		{
			return { qarma, zenity };
		}
		else // GNOME or XFCE, etc
		{
			return { zenity, qarma };
		}
	}

	static range dialog(table::span args)
	{
		// Look for any desktop utility program
		view program;
		static const auto session = pick();
		for (auto test : session)
		{
			auto path = which(test);
			if (path.begin() != path.end())
			{
				program = *path.begin();
				break;
			}
		}
		// Append the command line
		vector line;
		cache buf;
		line.push_back(program);
		for (auto arg : args)
		{
			auto [it, unique] = buf.emplace(fmt::opt::join(arg));
			#ifdef assert
			assert(unique);
			#endif
			line.emplace_back(it->data(), it->size());
		}
		return get(line);
	}

	range select(view path, view mode)
	{
		table::vector line {{ "file-selection", { } }};
		if (not path.empty())
		{
			line.emplace_back("filename", path);
		}
		if (not mode.empty())
		{
			line.emplace_back(mode, tag::empty);
		}
		return dialog(line);
	}

	range show(view text, view type)
	{
		table::vector line {{ type, { } }};
		if (not text.empty())
		{
			line.emplace_back("text", text);
		}
		return dialog(line);
	}

	range enter(view start, view label, bool hide)
	{
		table::vector line {{ "entry-text", start }};
		if (not label.empty())
		{
			line.emplace_back("text", label);
		}
		if (hide)
		{
			line.emplace_back("hide-text", tag::empty);
		}
		return dialog(line);
	}

	range text(view path, view check, view font, view type)
	{
		table::vector line {{ "text-info", { } }};
		if (type == "html")
		{
			line.emplace_back(type, tag::empty);
			line.emplace_back("url", path);
		}
		else
		{
			if (type == "editable")
			{
				line.emplace_back(type, tag::empty);
			}
			line.emplace_back("filename", path);
		}
		if (not font.empty())
		{
			line.emplace_back("font", font);
		}
		if (not check.empty())
		{
			line.emplace_back("checkbox", check);
		}
		return dialog(line);
	}

	range form(table::span add, view text, view title)
	{
		table::vector line {{ "forms", { } }};
		if (not text.empty())
		{
			line.emplace_back("text", text);
		}
		if (not title.empty())
		{
			line.emplace_back("title", title);
		}
		string prefix { "add-" };
		for (auto arg : add)
		{
			string copy = arg.second;
			auto key = prefix + copy;
			line.emplace_back(key, arg.first);
		}
		return dialog(line);
	}

	range notify(view text, view icon)
	{
		table::vector line {{ "notification", { } }};
		if (not text.empty())
		{
			line.emplace_back("text", text);
		}
		if (not icon.empty())
		{
			line.emplace_back("icon", icon);
		}
		return dialog(line);
	}

	range calendar(view text, view format, int day, int month, int year)
	{
		table::vector line {{ "calendar", { } }};
		if (not text.empty())
		{
			line.emplace_back("text", text);
		}
		if (not format.empty())
		{
			line.emplace_back("format", format);
		}
		if (0 < day)
		{
			const auto s = to_string(day);
			line.emplace_back("day", s);
		}
		if (0 < month)
		{
			const auto s = to_string(month);
			line.emplace_back("month", s);
		}
		if (0 < year)
		{
			const auto s = to_string(year);
			line.emplace_back("year", s);
		}
		return dialog(line);
	}

	range color(view start, bool palette)
	{
		table::vector line {{ "color-selection", { } }};
		if (not start.empty())
		{
			line.emplace_back("color", start);
		}
		if (palette)
		{
			line.emplace_back("show-palette", tag::empty);
		}
		return dialog(line);
	}
}

#ifdef TEST
TEST(cmd)
{
	const auto list = env::cmd::list(env::pwd());
	ASSERT(list.begin() != list.end());
	const auto read = env::cmd::read(__FILE__);
	ASSERT(read.begin() != list.end());
	// Skip forward
	auto it = read.begin();
	for (int n = 0; n < __LINE__; ++n) ++it;
	// Vector starts at 0, line numbering at 1
	ASSERT((*it).find("Recursive find me text") != fmt::npos);
}
TEST(echo)
{
	fmt::view user = env::got("ComSpec")
		? "%UserName%" : "$USER";

	const auto echo = env::cmd::echo(user);
	ASSERT(echo.begin() != echo.end());
	const auto name = *echo.begin();
	ASSERT(user != name);
}
#endif
