// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "err.hpp"
#include "sym.hpp"
#include "dir.hpp"
#include "env.hpp"
#include "type.hpp"

#ifdef _WIN32
#include "win.hpp"
#else
#include <dlfcn.h>
#endif

namespace sys
{
	lib & lib::bin()
	{
		static lib local;
		return local;
	}

	lib::lib(const char* path)
	{
		#ifdef _WIN32
		{
			const auto h = LoadLibrary(path);
			if (nullptr == h)
			{
				#ifdef WINERR
				WINERR("LoadLibrary", path);
				#endif
			}
			else ptr = h;
		}
		#else
		{
			ptr = dlopen(path, RTLD_LAZY);
			if (nullptr == ptr)
			{
				const auto e = dlerror();
				if (nullptr != e)
				{
					#ifdef WARN
					WARN(e);
					#endif
				}
			}
		}
		#endif
	}

	lib::~lib()
	{
		#ifdef _WIN32
		{
			const auto h = static_cast<HMODULE>(ptr);
			if (nullptr != h and not FreeLibrary(h))
			{
				#ifdef WINERR
				WINERR("FreeLibrary");
				#endif
			}
		}
		#else
		{
			if (nullptr != ptr and dlclose(ptr))
			{
				const auto e = dlerror();
				if (nullptr != e)
				{
					#ifdef WARN
					WARN(e);
					#endif
				}
			}
		}
		#endif
	}

	void *lib::sym(const char* name) const
	{
		#ifdef _WIN32
		{
			auto h = static_cast<HMODULE>(ptr);
			if (nullptr == h)
			{
				h = GetModuleHandle(nullptr);
				if (nullptr == h)
				{
					#ifdef WINERR
					WINERR("GetModuleHandle");
					#endif
					return nullptr;
				}
			}

			const auto f = GetProcAddress(h, name);
			if (nullptr == f)
			{
				#ifdef WINERR
				WINERR("GetProcAddress");
				#endif
			}
			return f;
		}
		#else
		{
			(void) dlerror();
			auto f = dlsym(ptr, name);
			const auto e = dlerror();
			if (nullptr != e)
			{
				#ifdef WARN
				WARN(e);
				#endif
			}
			return f;
		}
		#endif
	}

	lib lib::find(const char* basename)
	{
		using namespace env::file;
		auto stop = fwd::always<fmt::view>;
		auto dirs = env::path();
		auto name = fmt::to_string(basename);
		name += sys::tag::share;
		any_of(dirs, regex(name) || copy(name) || stop);
		return name.c_str();
	}
}

#ifdef TEST
static int hidden() { return 42; }
dynamic int visible() { return hidden(); }

TEST(sym)
{
	// Can see dynamic
	auto f = sys::sym<int()>("visible");
	ASSERT(nullptr != f);
	// Cannot see static
	auto g = sys::sym<int()>("hidden");
	ASSERT(nullptr == g);
	// Callable object
	ASSERT(f() == hidden());
}
#endif
