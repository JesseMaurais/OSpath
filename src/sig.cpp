// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "err.hpp"
#include "sig.hpp"
#include "sync.hpp"
#include <unordered_map>
#include <functional>
#include <csignal>
#include <cstring>
#include <queue>

namespace
{
	static sys::exclusive<std::unordered_map<std::string_view, fwd::event>> sigmap;
	static sys::exclusive<std::unordered_map<int, fwd::event>> sigev;
	static const std::unordered_map<std::string_view, int> sigint
	{
		#ifdef SIGABRT
		{ "abort", SIGABRT },
		#endif
		
		#ifdef SIGALRM
		{ "alarm", SIGALRM },
		#endif
		
		#ifdef SIGBUS
		{ "bus", SIGBUS },
		#endif
		
		#ifdef SIGCHLD
		{ "child", SIGCHLD },
		#endif
		
		#ifdef SIGCONT
		{ "continue", SIGCONT },
		#endif
		
		#ifdef SIGFPE
		{ "float", SIGFPE },
		#endif
		
		#ifdef SIGHUP
		{ "hangup", SIGHUP },
		#endif
		
		#ifdef SIGILL
		{ "illegal", SIGILL },
		#endif
		
		#ifdef SIGINT
		{ "interrupt", SIGINT },
		#endif
		
		#ifdef SIGKILL
		{ "kill", SIGKILL },
		#endif
		
		#ifdef SIGPIPE
		{ "pipe", SIGPIPE },
		#endif
		
		#ifdef SIGPOLL
		{ "poll", SIGPOLL },
		#endif
		
		#ifdef SIGQUIT
		{ "quit", SIGQUIT },
		#endif
		
		#ifdef SIGSEGV
		{ "segment", SIGSEGV },
		#endif
		
		#ifdef SIGSTOP
		{ "stop", SIGSTOP },
		#endif
		
		#ifdef SIGSYS
		{ "system", SIGSYS },
		#endif
		
		#ifdef SIGTERM
		{ "terminate", SIGTERM },
		#endif
		
		#ifdef SIGTSTP
		{ "pause", SIGTSTP },
		#endif
		
		#ifdef SIGTTIN
		{ "input", SIGTTIN },
		#endif
		
		#ifdef SIGTTOU
		{ "output", SIGTTOU },
		#endif
		
		#ifdef SIGTRAP
		{ "trap", SIGTRAP },
		#endif
		
		#ifdef SIGURG
		{ "urgent", SIGURG },
		#endif
		
		#ifdef SIGUSR1
		{ "user1", SIGUSR1 },
		#endif
		
		#ifdef SIGUSR2
		{ "user2", SIGUSR2 },
		#endif
		
		#ifdef SIGPROF
		{ "profile", SIGPROF },
		#endif
		
		#ifdef SIGVTALRM
		{ "virtual", SIGVTALRM },
		#endif
		
		#ifdef SIGXCPU
		{ "time", SIGXCPU },
		#endif
		
		#ifdef SIGXFSZ
		{ "size", SIGXFSZ },
		#endif
	};
	
	static void handler(int no)
	{
		#ifdef psignal
		psignal(no, __FILE__);
		#endif
	
		auto reader = sigev.reader();
		if (auto it = reader->find(no); reader->end() != it)
		{
			#ifdef assert
			assert(no == it->first);
			#endif
			std::invoke(it->second);
		}
	}
	
	static void error(int no)
	{
		sys::psignal(no, __FILE__);
	}
	
	static auto sighandler(fwd::event value)
	{
		using type = decltype(env::sig::def);
		if (value.target_type() == typeid(type))
		{
			const auto ptr = value.target<type>();
			if (ptr == env::sig::def) return SIG_DFL;
			if (ptr == env::sig::ign) return SIG_IGN;
			if (ptr == env::sig::err) return error;
		}
		return handler;
	}
}

namespace env::sig
{
	void set(view key, event value)
	{
		auto f = sighandler(value);
		
		if (auto it = sigint.find(key); sigint.end() != it)
		{
			#ifdef assert
			assert(key == it->first);
			#endif
			
			if (SIG_ERR == std::signal(it->second, f))
			{
				#ifdef perror
				perror("signal", key);
				#else
				perror("signal");
				#endif
			}

			auto writer = sigev.writer();
			if (handler == f)
			{
				writer->emplace(it->second, value);
			}
			else
			{
				writer->erase(it->second);
			}
		}
		
		auto writer = sigmap.writer();
		if (handler == f)
		{
			writer->emplace(key, value);
		}
		else
		{
			writer->erase(key);
		}
	}
	
	event get(view key)
	{
		auto reader = sigmap.reader();
		if (auto it = reader->find(key); reader->end() != it)
		{
			#ifdef assert
			assert(key == it->first);
			#endif
			return it->second;
		}
		return { };
	}
	
	void raise(view key)
	{
		if (auto f = get(key))
		{
			std::invoke(f);
		}
		else
		{
			#ifdef WARN
			WARN("Key not found:", key);
			#endif
		}
	}
	
	void all(event value)
	{
		for (auto [key, num] : sigint)
		{
			set(key, value);
			(void) num;
		}
	}
	
	sys::exclusive<std::queue<event>> queue;
	
	void push(event ev)
	{
		queue.writer()->push(ev);
	}
	
	event pop()
	{
		auto writer = queue.writer();
		auto back = writer->back();
		writer->pop();
		return back;
	}
	
	void def()
	{
		#ifdef assert
		assert(false and "Not to be called");
		#endif
	}
		
	void ign()
	{
		#ifdef assert
		assert(false and "Not to be called");
		#endif
	}
		
	void err()
	{
		#ifdef assert
		assert(false and "Not to be called");
		#endif
	}
}
