// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "err.hpp"
#include "fmt.hpp"
#include "env.hpp"
#include "arg.hpp"
#include "usr.hpp"
#include "dir.hpp"
#include "cmd.hpp"
#include "sys.hpp"
#include "type.hpp"
#include "sync.hpp"

namespace env
{
	view echo(view line)
	{
		thread_local buffer buf;
		for (auto out : cmd::echo(line))
		{
			buf << out << tag::eol;
		}
		return buf.str();
	}

	static auto catalog()
	{
		auto app = opt::program();
		return catalog(app);
	}

	view cat(view line)
	{
		static auto convert = catalog();
		return convert(line);
	}

	view pwd()
	{
		static thread_local char buf[FILENAME_MAX];
		return sys::getcwd(buf, sizeof buf);
	}

	span path()
	{
		static thread_local vector buf;
		const auto value  = get("PATH");
		buf = fmt::path::split(value);
		return buf;
	}

	view home()
	{
		#ifdef _WIN32
		return get("UserHome");
		#else
		return get("HOME");
		#endif
	}

	view user()
	{
		#ifdef _WIN32
		return get("UserName");
		#else
		return get("USER");
		#endif
	}

	view base()
	{
		#ifdef _WIN32
		return get("SystemDrive");
		#else
		return { };
		#endif
	}

	view root()
	{
		#ifdef _WIN32
		return get("SystemRoot");
		#else
		return "/root";
		#endif
	}

	view shell()
	{
		#ifdef _WIN32
		return get("ComSpec");
		#else
		return get("SHELL");
		#endif
	}

	view session()
	{
		#ifdef _WIN32
		return get("OS");
		#else
		return get("DESKTOP_SESSION");
		#endif
	}

	view host()
	{
		#ifdef _WIN32
		return get("ComputerName");
		#else
		{
			static thread_local char buf[64];
			if (gethostname(buf, sizeof buf))
			{
				std::memset(buf, 0, sizeof buf);
				perror("gethostname");
			}
			return buf;
		}
		#endif
	}
 
	view domain()
	{
		#ifdef _WIN32
		return get("UserDomain");
		#else
		{
			static thread_local char buf[64];
			if (getdomainname(buf, sizeof buf))
			{
				std::memset(buf, 0, sizeof buf);
				perror("getdomainname");
			}
			return buf;
		}
		#endif
	}

	view temp()
	{
		for (auto name : { "TMPDIR", "TEMP", "TMP" })
		{
			auto value = get(name);
			if (not value.empty())
			{
				return value;
			}
		}
		return { };
	}

	static sys::rwlock lock; // for environ global

	bool got(view name)
	{
		if (not terminated(name))
		{
			const auto buf = to_string(name);
			return got(buf);
		}
		const auto key = lock.reader();
		const auto ptr = std::getenv(name.data());
		return nullptr == ptr;
	}

	view get(view name)
	{
		if (not terminated(name))
		{
			const auto buf = to_string(name);
			return get(buf);
		}
		const auto key = lock.reader();
		const auto ptr = std::getenv(name.data());
		return nullptr == ptr ? tag::empty : ptr;
	}

	bool set(view name)
	{
		if (not terminated(name))
		{
			return put(name);
		}
		const auto key = lock.writer();
		const auto ptr = fwd::non_const(name.data());
		if (sys::putenv(ptr))
		{
			#ifdef perror
			perror("putenv", name);
			#else
			perror("putenv");
			#endif
			return failure;
		}
		return success;
	}

	bool put(view name)
	{
		static cache buf;
		const auto key = lock.writer();
		const auto it = buf.emplace(name).first;
		const auto ptr = fwd::non_const(it->data());
		if (sys::putenv(ptr))
		{
			#ifdef perror
			perror("putenv", name);
			#else
			perror("putenv");
			#endif
			return failure;
		}
		return success;
	}
}

#ifdef TEST
TEST(path)
{
	ASSERT(env::get("PATH") == fmt::path::join(env::path()));
	ASSERT(env::get("PATH") == env::echo("PATH"));
}
#endif
