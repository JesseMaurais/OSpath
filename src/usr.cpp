// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "err.hpp"
#include "usr.hpp"
#include "env.hpp"
#include "opt.hpp"
#include "arg.hpp"
#include "dir.hpp"
#include "type.hpp"
#include "file.hpp"
#include <fstream>

#ifdef _WIN32
# include <shlobj.h>
#endif

namespace env::usr
{
	view dir(view name)
	{
		auto path = tag::empty;

	#ifdef _WIN32

		using entry = std::pair<KNOWNFOLDERID, view>;

		static const std::map<view, entry> map =
		{
			{ "AccountPictures", entry{FOLDERID_AccountPictures, "%AppData%\\Microsoft\\Windows\\AccountPictures"}},
			{ "AdminTools", entry{FOLDERID_AdminTools, "%AppData%\\Microsoft\\Windows\\Start Menu\\Programs\\Administrative Tools;%UserProfile%\\Start Menu\\Programs\\Administrative Tools"}},
			{ "AppDataDesktop", entry{FOLDERID_AppDataDesktop, "%LocalAppData%\\Desktop"}},
			{ "AppDataDocuments", entry{FOLDERID_AppDataDocuments, "%LocalAppData%\\Documents"}},
			{ "AppDataFavorites", entry{FOLDERID_AppDataDocuments, "%LocalAppData%\\Favorites"}},
			{ "AppDataProgramData", entry{FOLDERID_AppDataProgramData, "%LocalAppData%\\ProgramData"}},
			{ "ApplicationShortcuts", entry{FOLDERID_ApplicationShortcuts, "%LocalAppData%\\Microsoft\\Windows\\Application Shortcuts"}},
			{ "CameraRoll", entry{FOLDERID_CameraRoll, "%UserProfile%\\Pictures\\Camera Roll"}}
			{ "CDBurning", entry{FOLDERID_CDBurning, "%LocalAppData%\\Microsoft\\Windows\\Burn\\Burn;%UserProfile%\\Local Settings\\Application Data\\Microsoft\\CD Burning"}},
			{ "CommonAdminTools", entry{FOLDERID_CommonAdminTools, "%AllUsersProfile%\\Microsoft\\Windows\\Start Menu\\Programs\\Administrative Tools;%AllUsersProfile%\\Start Menu\\Programs\\Administrative Tools"}},
			{ "CommonOEMLinks", entry{FOLDERID_CommonOEMLinks, "%AllUsersProfile%\\OEM Links"}},
			{ "CommonPrograms", entry{FOLDERID_CommonStartMenu, "%AllUsersProfile%\\Microsoft\\Windows\\Start Menu\\Programs;%AllUsersProfile%\\Start Menu\\Programs"}},
			{ "CommonStartMenu", entry{FOLDERID_CommonStartMenu, "%AllUsersProfile%\\Microsoft\\Windows\\Start Menu;%AllUsersProfile%\\Start Menu"}},
			{ "CommonStartup", entry{FOLDERID_CommonStartup, "%AllUsersProfile%\\Microsoft\\Windows\\Start Menu\\Programs\\StartUp;%AllUsersProfile%\\Start Menu\\Programs\\StartUp"}},
			{ "CommonTemplates" {FOLDERID_CommonTemplates, "%AllUsersProfile%\\Microsoft\\Windows\\Templates;%AllUsersProfile%\\Templates"}},
			{ "Contacts", entry{FOLDERID_Contacts, "%UserProfile%\\Contacts"}},
			{ "Cookies", entry{FOLDERID_Cookies, "%AppData%\\Microsoft\\Windows\\Cookies"}},
			{ "Desktop", entry{FOLDERID_Desktop, "%UserProfile%\\Desktop"}},
			{ "DeviceMetadataStore", entry{FOLDERID_DeviceMetadataStore, "%AllUsersProfile%\\Microsoft\\Windows\\DeviceMetadataStore"}},
			{ "Documents", entry{FOLDERID_Documents, "%UserProfile%\\Documents;%UserProfile%\\My Documents"}},
			{ "Downloads", entry{FOLDERID_Downloads, "%UserProfile%\\Downloads"}},
			{ "Favorites", entry{FOLDERID_Favorites, "%UserProfile%\\Favorites"}},
			{ "Fonts", entry{FOLDERID_Fonts, "%WinDir%\\Fonts" }},
			{ "History", entry{FOLDERID_History, "%LocalAppData%\\Microsoft\\Windows\\History;%userProfile%\\Local Settings\\History"}},
			{ "Libraries", entry{FOLDERID_Libraries, "%AppData%\\Microsoft\\Windows\\Libraries"}},
			{ "Links", entry{FOLDERID_Links, "%UserProfile%\\Links"}},
			{ "LocalAppData", entry{FOLDERID_LocalAppData, "%LocalAppData%;%UserProfile%\\Local Settings\\Application Data"}},
			{ "Music", entry{FOLDERID_Music, "%UserProfile%\\Music;%UserProfile%\\My Documents\\My Music"}},
			{ "NetHood", entry{FOLDERID_NetHood, "%AppData%\\Microsoft\\Windows\\Network Shortcuts;%UserProfile%\\NetHood"}},
			{ "Objects3D", entry{FOLDERID_Objects3D, "%UserProfile%\\3D Objects"}},
			{ "OriginalImages", entry{FOLDERID_OriginalImages, "%LocalAppData%\\Microsoft\\Windows\\Photo Gallery\\Original Images"}},
			{ "PhotoAlbums", entry{FOLDERID_PhotoAlbums, "%UserProfile%\\Pictures\\Slide Shows"}}
			{ "Pictures", entry{FOLDERID_Pictures, "%UserProfile%\\Pictures;%UserProfile%\\My Documents\\My Pictures"}},
			{ "Playlists", entry{FOLDERID_Playlists, "%UserProfile%\\Music\\Playlists"}},
			{ "PrintHood", entry{FOLDERID_PrintHood, "%AppData%\\Microsoft\\Windows\\Printer Shortcuts;%UserProfile%\\PrintHood"}},
			{ "Profile", entry{FOLDERID_Profile, "%UserProfile%;%SystemDrive%\\Users\\%UserName%;%SystemDrive%\\Documents and Settings\\%UserName%"}},
			{ "ProgramData", entry{FOLDERID_ProgramData, "%AllUsersProfile%;%ProgramData%;%SystemDrive%\\ProgramData;%AllUsersProfile%\\Application Data"}},
			{ "ProgramFiles", entry{FOLDERID_ProgramFiles, "%ProgramFiles%;%SystemDrive%\\Program Files"}},
			{ "ProgramFilesCommon", entry{FOLDERID_ProgramFilesCommon, "%ProgramFiles%\\Common Files"}},
			{ "Programs", entry{FOLDERID_Programs, "%AppData%\\Microsoft\\Windows\\Start Menu\\Programs;%UserProfile%\\Start Menu\\Programs"}},
			{ "Public", entry{FOLDERID_Public, "%Public%;%SystemDrive%\\Users\\Public"}},
			{ "PublicDesktop", entry{FOLDERID_PublicDesktop, "%Public%\\Desktop;%AllUsersProfile%\\Desktop"}},
			{ "PublicDocuments", entry{FOLDERID_PublicDocuments, "%Public%\\Documents;%AllUsersProfile%\\Documents"}},
			{ "PublicDownloads", entry{FOLDERID_PublicDownloads, "%Public%\\Downloads"}},
			{ "PublicLibraries", entry{FOLDERID_PublicLibraries, "%AllUsersProfile%\\Microsoft\\Windows\\Libraries"}},
			{ "PublicMusic", entry{FOLDERID_PublicMusic, "%Public%\\Music;%AllUsersProfile%\\Documents\\My Music"}},
			{ "PublicPictures", entry{FOLDERID_PublicPictures, "%Public%\\Pictures;%AllUsersProfile%\\Documents\\My Pictures"}},
			{ "PublicRingtones", entry{FOLDERID_PublicRingtones, "%AllUsersProfile%\\Microsoft\\Windows\\Ringtones"}},
			{ "PublicVideos", entry{FOLDERID_PublicVideos, "%Public%\\Videos;%AllUsersProfile%\\Documents\\My Videos"}},
			{ "Recent", entry{FOLDERID_Recent, "%AppData%\\Microsoft\\Windows\\Recent;%UserProfile%\\Recent"}},
			{ "ResourceDir", entry{FOLDERIDID_ResourceDir, "%WinDir%\\Resources"}},
			{ "Ringtones", entry{FOLDERID_Ringtones, "%LocalAppData%\\Microsoft\\Windows\\Ringtones"}},
			{ "RoamingAppData", entry{FOLDERID_RoamingAppData, "%AppData%;%UserProfile%\\AppData\\Roaming;%UserProfile%\\Application Data"}},
			{ "Screenshots", entry{FOLDERID_Screenshots, "%UserProfile%\\Pictures\\Screenshots"}},
			{ "StartMenu", entry{FOLDERID_StartMenu, "%AppData%\\Microsoft\\Windows\\Start Menu;%UserProfile%\\Start Menu"}},
			{ "Startup", entry{FOLDERID_Startup, "%AppData%\\Microsoft\\Windows\\Start Menu\\Programs\\StartUp;%UserProfile%\\Start Menu\\Programs\\StartUp"}},
			{ "System", entry{FOLDERID_System, "%WinDir%\\system32"}},
			{ "Templates", entry{FOLDERID_Templates, "%AppData%\\Microsoft\\Windows\\Templates;%UserProfile%\\Templates"}},
			{ "UserProfiles", entry{FOLDERID_UserProfiles, "%SystemDrive%\\Users"}},
			{ "UserProgramFiles", entry{FOLDERID_UserProgramFiles, "%LocalAppData%\\Programs"}},
			{ "UserProgramFilesCommon", entry{FOLDERID_UserProgramFilesCommon, "%LocalAppData%\\Programs\\Common"}},
			{ "Videos", entry{FOLDERID_Videos, "%UserProfile%\\Videos;%UserProfile%\\My Documents\\My Videos"}},
			{ "Windows", entry{FOLDERID_Windows, "%WinDir%"}}
		};

		if (auto it = map.find(name); map.end() != it)
		{
			#ifdef assert
			assert(name == it->first);
			#endif

			auto [id, paths] = it->second;

			PWSTR pws = nullptr;
			const auto ok = SHGetKnownFolderPath
			(
				id, KF_FLAG_DEFAULT, nullptr, &pws
			);

			if (S_OK == ok)
			{
				thread_local string buf;
				buf = to_string(pws);
				path = buf;
			}

			if (nullptr != pws)
			{
				CoTaskMemFree(pws);
			}

			if (path.empty())
			{
				path = paths;
			}
		}

	#else

		static const std::map<view, pair> map =
		{
			{ "Cache-Home", { "XDG_CACHE_HOME", "$HOME/.cache" }},
			{ "Config-Dirs", { "XDG_CONFIG_DIRS", "/etc/xdg" }},
			{ "Config-Home", { "XDG_CONFIG_HOME", "$HOME/.config" }},
			{ "Data-Dirs", { "XDG_DATA_DIRS", "/usr/local/share:/usr/share" }},
			{ "Data-Home", { "XDG_DATA_HOME", "$HOME/.local/share" }},
			{ "Desktop", { "XDG_DESKTOP_DIR", "$HOME/Desktop" }},
			{ "Documents", { "XDG_DOCUMENTS_DIR", "$HOME/Documents" }},
			{ "Downloads", { "XDG_DOWNLOAD_DIR", "$HOME/Downloads" }},
			{ "Music", { "XDG_MUSIC_DIR", "$HOME/Music" }},
			{ "Runtime", { "XDG_RUNTIME_DIR", "$TMPDIR:$TEMP:$TMP" }},
			{ "Pictures", { "XDG_PICTURES_DIR", "$HOME/Pictures" }},
			{ "Public", { "XDG_PUBLICSHARE_DIR", "$HOME/Public" }},
			{ "State-Home", { "XDG_STATE_HOME", "$HOME/.local/state" }},
			{ "Templates", { "XDG_TEMPLATES_DIR", "$HOME/Templates" }},
			{ "Videos", { "XDG_VIDEOS_DIR", "$HOME/Videos" }},
		};

		static string config;
		if (config.empty())
		{
			auto paths = user_dirs();
			for (fmt::view path : paths)
			{
				auto buf = fmt::to_string(path);
				std::ifstream file(buf);
				if (file)
				{
					while (file >> opt::get);
					#ifdef assert
					assert(file.good());
					#endif
				}
			}
		}

		if (auto it = map.find(name); map.end() != it)
		{
			#ifdef assert
			assert(name == it->first);
			#endif

			auto [id, sym] = it->second;
			path = opt::get(id, sym);
		}

	#endif

		if (path.empty())
		{
			path = get(name);
		}
		return echo(path);
	}

	span user_dirs()
	{
		static vector buf;
		if (buf.empty())
		{
			const init list
			#ifdef _WIN32
				{ "%ProgramData%\\user-dirs.defaults", "%AppData%\\user-dirs.dir" };
			#else
				{ "/etc/xdg/user-dirs.defaults", "$HOME/.config/user-dirs.dir" };
			#endif
			for (auto line : list)
			{
				auto path = echo(line);
				path = tag::put(path);
				buf.emplace_back(path);
			}
		}
		return buf;
	}

	view current_desktop()
	{
		auto name = get("XDG_CURRENT_DESKTOP");
		if (name.empty())
		{
			name = session();
		}
		return name;
	}

	static view menu_prefix()
	{
		auto name = get("XDG_MENU_PREFIX");
		if (name.empty())
		{
			name = current_desktop();
		}
		else
		if (name.back() == '-')
		{
			name = name.substr(0, name.size() - 1);
		}
		return name;
	}

	view applications_menu()
	{
		thread_local string path;
		if (path.empty())
		{
			const view prefix = menu_prefix();
			const view basename = "applications.menu";
			vector parts { prefix, basename };
			const auto file = fmt::join(parts, tag::dash);
			const auto found = env::file::any_of(config(), [&](auto dir)
			{
				path = fmt::dir::join(dir, "menus", file);
				return not env::file::access(path);
			});

			if (not found)
			{
				path.clear();
			}
		}
		return path;
	}

	static auto expand(init list)
	{
		vector buf;
		for (auto name : list)
		{
			auto lines = fmt::path::split(dir(name));
			for (auto path : lines)
			{
				path = tag::put(path);
				buf.emplace_back(path);
			}
		}
		return buf;
	}

	span runtime()
	{
		static vector buf;
		if (buf.empty())
		{
			const init list
			#ifdef _WIN32
				{ "AppData", "LocalAppData" };
			#else
				{ "Runtime" };
			#endif
			buf = expand(list);
		}
		return buf;
	}

	span config()
	{
		static vector buf;
		if (buf.empty()) 
		{
			const init list
			#ifdef _WIN32
				{ "AppData", "LocalAppData" };
			#else
				{ "Config-Home", "Config-Dirs" };
			#endif
			buf = expand(list);
		}
		return buf;
	}

	span cache()
	{
		static vector buf;
		if (buf.empty())
		{
			const init list
			#ifdef _WIN32
				{ "ProgramData" };
			#else
				{ "Cache-Home" };
			#endif
			buf = expand(list);
		}
		return buf;
	}

	span data()
	{
		static vector buf;
		if (buf.empty())
		{
			const init list
			#ifdef _WIN32
				{ "AppData", "LocalAppData" };
			#else
				{ "Data-Home", "Data-Dirs" };
			#endif
			buf = expand(list);
		}
		return buf;
	}
}

#ifdef TEST
namespace
{
	fmt::output format(fmt::output out, fmt::pair brace, fmt::view mid)
	{
		return out << brace.first << mid << brace.second << fmt::tag::eol;
	}

	fmt::write group(fmt::view name)
	{
		return [=](fmt::output out) -> fmt::output
		{
			return format(out, fmt::tag::square, name);
		};
	}

	//fmt::output operator<<(fmt::output out, key& obj)

	fmt::write key(fmt::view name, fmt::view value)
	{
		return [=](fmt::output out) -> fmt::output
		{
			return format(out, { name, value }, fmt::tag::equal);
		};
	}
}

TEST(usr)
{
	std::ofstream out { ".ini" };

	out << group("Fake Environment")
	    << key("home", env::home())
	    << key("user", env::user())
	    << key("root", env::root())
	    << key("host", env::host())
	    << key("pwd", env::pwd())
	    << key("shell", env::shell())
	    << key("tmpdir", env::temp())
	    << key("rootdir", env::base())
	    << key("session", env::session())
	    << key("domain", env::domain())
	    << std::endl;

	out << group("Desktop")
	    << key("runtime", fmt::path::join(env::usr::runtime()))
	    << key("config", fmt::path::join(env::usr::config()))
	    << key("cache", fmt::path::join(env::usr::cache()))
	    << key("data", fmt::path::join(env::usr::data()))
	    << std::endl;

	out << group("User Directories")
	    << key("desktop", env::usr::desktop())
	    << key("documents", env::usr::documents())
	    << key("download", env::usr::download())
	    << key("music", env::usr::music())
	    << key("pictures", env::usr::pictures())
	    << key("public", env::usr::share())
	    << key("template", env::usr::templates())
	    << key("videos", env::usr::videos())
	    << std::endl;

	out << group("Application Options")
	    << key("name", env::opt::application())
	    << key("program", env::opt::program())
	    << key("command-line", fmt::join(env::opt::arguments(), " "))
	    << key("config", env::opt::config())
	    << std::endl;

	out << env::opt::set << std::endl;
}
#endif
