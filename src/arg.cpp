// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "err.hpp"
#include "env.hpp"
#include "opt.hpp"
#include "arg.hpp"
#include "dig.hpp"
#include "ini.hpp"
#include "dir.hpp"
#include "usr.hpp"
#include "algo.hpp"
#include "type.hpp"
#include "sync.hpp"
#include <fstream>

namespace fmt::opt
{
	static void join(output buf, pair part, pair type)
	{
		auto quote = any_of(part.second)
			? tag::quote : tag::empty;
		buf
			<< type.first << part.first << type.second
			<< quote << part.second << quote
			<< tag::space;
	}

	string join(pair part, pair type)
	{
		buffer buf;
		join(buf, part, type);
		return buf.str();
	}

	string join(span list, pair type)
	{
		buffer buf;
		for (auto part : list)
		{
			join(buf, part, type);
		}
		return buf.str();
	}

	string join(init list, pair type)
	{
		auto ptr = fwd::to_span(list);
		return join(ptr, type);
	}
}

namespace
{
	static auto make_key(fmt::view key)
	{
		return std::make_pair(fmt::tag::empty, key);
	}

	static auto find_next(fmt::view argu, env::opt::cmd::span cmd)
	{
		auto next = cmd.end();

		if (argu.starts_with(fmt::tag::dual))
		{
			next = fwd::find_if
			(
				cmd, [=](const auto& d)
				{
					return d.name == argu.substr(2);
				}
			);
		}
		else
		if (argu.starts_with(fmt::tag::dash))
		{
			next = fwd::find_if
			(
				cmd, [=](const auto& d)
				{
					return d.dash == argu.substr(1);
				}
			);
		}

		return next;
	}
	
	template <class Stream> static auto open()
	{
		auto path = env::opt::config();
		assert(fmt::terminated(path));
		return Stream(path.data());
	}

	static auto registry()
	{
		struct init : doc::ini
		{
			init()
			{
				auto input = open<std::ifstream>();
				while (input >> *this);
			}
			~init()
			{
				auto output = open<std::ofstream>();
				while (output << *this);
			}
		};
	
		static sys::exclusive<init> ini;
		return std::addressof(ini);
	}
}

namespace env::opt
{
	static vector list;

	view application()
	{
		constexpr auto key = "Application";
		return got(key) ? get(key) : program();
	}

	span arguments()
	{
		return { list.data(), list.size() };
	}

	view program()
	{
		static view name;
		if (name.empty())
		{
			auto const args = arguments();
			assert(not args.empty());
			auto const path = args.front();
			assert(not path.empty());
			auto const dirs = dir::split(path);
			assert(not dirs.empty());
			auto const file = dirs.back();
			assert(not file.empty());
			auto const first = file.find_first_not_of("./");
			auto const last = file.rfind(sys::tag::image);
			name = file.substr(first, last);
		}
		return name;
	}

	view config()
	{
		static string buf;
		if (buf.empty())
		{
			const auto name = fmt::file::join(program(), "ini");
			for (auto dir : env::usr::config())
			{
				using namespace env::file;
				auto stop = fwd::always<view>;
				if (env::file::any_of(dir, regex(name) || copy(buf) || stop))
				{
					break;
				}
			}
		}
		return buf;
	}

	bool got(pair key)
	{
		return registry()->reader()->got(key);
	}

	view get(pair key)
	{
		return registry()->reader()->get(key);
	}

	bool set(pair key, view value)
	{
		return registry()->writer()->set(key, value);
	}

	bool got(view key)
	{
		return registry()->reader()->got(make_key(key));
	}

	view get(view key)
	{
		return registry()->reader()->get(make_key(key));
	}

	bool set(view key, view value)
	{
		return registry()->writer()->set(make_key(key), value);
	}

	vector parse(int argc, char** argv, cmd::span par)
	{
		// Push a view to command line arguments
		std::copy(argv, argv + argc, std::back_inserter(list));
		// Arguments not part of a command
		vector extra, args;
		// Command line iterator
		const auto end = par.end();
		auto current = end;
		// Skip the program image path in first position
		for (view argu : std::span(argv + 1, argc - 1))
		{
			// Check whether it is a new command parameter
			if (auto next = find_next(argu, par); end != next)
			{
				// Set as option
				const auto key = tag::put(next->name);
				current = 0 < next->argn ? next : end;
				set(key, true);
				args.clear();
			}
			else
			if (end != current)
			{
				const auto size = to_size(current->argn);
				if (args.size() < size)
				{
					// Set as option
					args.emplace_back(argu);
				}
				else
				{
					const auto value = fmt::join(args, ";");
					const auto key = tag::put(current->name);
					set(key, value);
					current = end;
					args.clear();
				}
			}
			else
			{
				extra.emplace_back(argu);
			}
		}
		return extra;
	}

	output set(output out)
	{
		auto reader = registry()->reader();
		return out << *reader;
	}

	input get(input in)
	{
		auto writer = registry()->writer();
		return in >> *writer;
	}
}

#ifdef TEST
TEST(arg)
{
	// Application name exists
	{
		const auto app = env::opt::program();
		ASSERT(not app.empty() and "Application is not named");
	}
}
#endif
