// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "err.hpp"
#include "mode.hpp"
#include "file.hpp"
#include "dir.hpp"
#include "env.hpp"
#include "usr.hpp"
#include "ptr.hpp"
#include "sys.hpp"
#include "sync.hpp"
#include "type.hpp"
#include "algo.hpp"
#include <climits>
#include <bitset>

#ifdef _WIN32
#include "win/file.hpp"
#include "win/memory.hpp"
#else
#include "uni/dirent.hpp"
#include "uni/fcntl.hpp"
#include "uni/mman.hpp"
#endif

namespace env::file
{
	int to_flags(view mode)
	{
		enum : size_t
		{
			rd, wr, app, ext, bin, txt, xu, sz
		};

		std::bitset<sz> bit;

		for (char op : mode) switch (op)
		{
			case 'r':
			bit.set(rd);
			continue;

			case 'w':
			bit.set(wr);
			continue;

			case 'a':
			bit.set(app);
			continue;

			case '+':
			bit.set(ext);
			continue;

			case 'b':
			bit.set(bin);
			continue;

			case 't':
			bit.set(txt);
			continue;

			case 'x':
			bit.set(xu);
			continue;

			default:
			#ifdef assert
			assert("Illegal char");
			#endif
		}

		int flags = 0;

		if (bit.test(ext))
		{
			if (bit.test(app))
			{
				flags |= O_RDWR | O_CREAT | O_APPEND;
			}
			else
			if (bit.test(wr))
			{
				flags |= O_RDWR | O_CREAT | O_TRUNC;
			}
			else
			if (bit.test(rd))
			{
				flags |= O_RDWR;
			}
		}
		else
		{
			if (bit.test(app))
			{
				flags |= O_WRONLY | O_CREAT | O_APPEND;
			}
			else
			if (bit.test(wr))
			{
				flags |= O_WRONLY | O_CREAT | O_TRUNC;
			}
			else
			if (bit.test(rd))
			{
				flags |= O_RDONLY;
			}
		}

		if (bit.test(bin))
		{
			flags |= O_BINARY;
		}

		if (bit.test(txt))
		{
			flags |= O_TEXT;
		}

		if (bit.test(xu))
		{
			flags |= O_EXCL;
		}

		return flags;
	}

	mode_t to_mode(view mode, mode_t flags)
	{
		char who = 'u', how = '+';
		for (char op : mode)
		{
			int mask = 0;
			switch (op)
			{

			case ',':
			continue;

			case 'u':
			case 'g':
			case 'o':
			case 'a':
			who = op;
			continue;

			case '=':
			{
				if (fmt::any_of("au", who))
				{
					flags &= ~S_IRWXU;
				}
				if (fmt::any_of("ag", who))
				{
					flags &= ~S_IRWXG;
				}
				if (fmt::any_of("ao", who))
				{
					flags &= ~S_IRWXO;
				}
			}
			case '+':
			case '-':
			how = op;
			continue;

			case 'r':
			{
				if (fmt::any_of("au", who))
				{
					mask |= S_IRUSR;
				}
				if (fmt::any_of("ag", who))
				{
					mask |= S_IRGRP;
				}
				if (fmt::any_of("ao", who))
				{
					mask |= S_IROTH;
				}
			}
			goto apply;

			case 'w':
			{
				if (fmt::any_of("au", who))
				{
					mask |= S_IWUSR;
				}
				if (fmt::any_of("ag", who))
				{
					mask |= S_IWGRP;
				}
				if (fmt::any_of("ao", who))
				{
					mask |= S_IWOTH;
				}
			}
			goto apply;

			case 'x':
			{
				if (fmt::any_of("au", who))
				{
					mask |= S_IXUSR;
				}
				if (fmt::any_of("ag", who))
				{
					mask |= S_IXGRP;
				}
				if (fmt::any_of("ao", who))
				{
					mask |= S_IXOTH;
				}
			}
			goto apply;

			apply:
			if ('-' == how)
			{
				flags &= ~mask;
			}
			else
			{
				flags |= mask;
			}
			break;

			default:
			#ifdef assert
			assert("Illegal char");
			#endif
			}
		}
		return flags;
	}

	string from_flags(int flags)
	{
		string buf;

		if (flags & O_RDWR)
		{
			if (flags & O_APPEND)
			{
				buf = "a+";
			}
			else
			if (flags & O_TRUNC)
			{
				buf = "w+";
			}
			else
			{
				buf = "r+";
			}
		}
		else
		if (flags & O_WRONLY)
		{
			if (flags & O_APPEND)
			{
				buf = "a";
			}
			else
			{
				buf = "w";
			}
		}
		else
		if (flags & O_RDONLY)
		{
			buf = "r";
		}
		else
		{
			#ifdef assert
			assert("Invalid flags");
			#endif
		}

		if (flags & O_BINARY)
		{
			buf += "b";
		}
		else
		if (flags & O_TEXT)
		{
			buf += "t";
		}

		if (flags & O_EXCL)
		{
			buf += "x";
		}

		return buf;
	}

	string from_mode(mode_t flags, view prefix)
	{
		constexpr int bits[][3] =
		{
			{ S_IRUSR, S_IWUSR, S_IXUSR },
			{ S_IRGRP, S_IWGRP, S_IXGRP },
			{ S_IROTH, S_IWOTH, S_IXOTH },
		};

		auto mode = to_string(prefix);
		for (auto [r, w, x] : bits)
		{
			mode += (flags & r) ? "r" : "-";
			mode += (flags & w) ? "w" : "-";
			mode += (flags & x) ? "x" : "-";
		}
		return mode;
	}

	string stat(view path)
	{
		if (not terminated(path))
		{
			auto buf = to_string(path);
			return stat(buf);
		}

		sys::stat_t st[1];
		if (sys::lstat(path.data(), st))
		{
			#ifdef perror
			perror("lstat", path);
			#else
			perror("lstat");
			#endif
			return { };
		}

		char type = '-';

		#ifdef S_ISFBLK
		if (S_ISFBLK(st->st_mode))
		{
			type = 'b';
		}
		#endif

		#ifdef S_ISCHR
		if (S_ISCHR(st->st_mode))
		{
			type = 'c';
		}
		#endif

		#ifdef S_ISDIR
		if (S_ISDIR(st->st_mode))
		{
			type = 'd';
		}
		#endif

		#ifdef S_ISFIFO
		if (S_ISFIFO(st->st_mode))
		{
			type = 'f';
		}
		#endif

		#ifdef S_ISREG
		if (S_ISREG(st->st_mode))
		{
			type = 'g';
		}
		#endif

		#ifdef S_ISLNK
		if (S_ISLNK(st->st_mode))
		{
			type = 'l';
		}
		#endif

		#ifdef S_ISSOCK
		if (S_ISSOCK(st->st_mode))
		{
			type = 's';
		}
		#endif

		#ifdef S_TYPEISMQ
		if (S_TYPEISMQ(st))
		{
			type = 'q';
		}
		#endif

		#ifdef S_TYPEISSEM
		if (S_TYPEISSEM(st))
		{
			type = 'm';
		}
		#endif

		#ifdef S_TYPEISSHM
		if (S_TYPEISSHM(st))
		{
			type = 'h';
		}
		#endif

		#ifdef S_TYPEISTMO
		if (S_TYPEISTMO(st))
		{
			type = 'o';
		}
		#endif

		auto prefix = fmt::to_string(type);
		return from_mode(st->st_mode, prefix);
	}

	bool access(view path, view mode)
	{
		if (not terminated(path))
		{
			auto buf = to_string(path);
			return access(buf, mode);
		}

		enum : size_t { rd, wr, ex, sz };

		std::bitset<sz> bit;

		for (char op : mode) switch (op)
		{
			case '-':
			continue;

			case 'r':
			bit.set(rd);
			continue;

			case 'w':
			bit.set(wr);
			continue;

			case 'x':
			bit.set(ex);
			continue;

			default:
			#ifdef assert
			assert("Invalid char");
			#endif
		}

		#ifdef _WIN32
		if (bit.test(ex))
		{
			if (DWORD type[1]; GetBinaryType(path.data(), type))
			{
				#ifdef INFO
				switch (type[0])
				{
				case SCS_32BIT_BINARY:
					INFO("WIN32");
					break;
				case SCS_DOS_BINARY:
					INFO("DOS");
					break;
				case SCS_WOW_BINARY:
					INFO("WOW");
					break;
				case SCS_PIF_BINARY:
					INFO("PIF");
					break;
				case SCS_POSIX_BINARY:
					INFO("POSIX");
					break;
				case SCS_OS216_BINARY:
					INFO("OS/2");
					break;
				case SCS_64BIT_BINARY:
					INFO("WIN64");
					break;
				};
				#endif
			}
			else
			{
				#ifdef WINERR
				WINERR("GetBinaryType", path);
				#endif
				return failure;
			}
		}
		#endif

		int flags = F_OK;

		if (bit.test(rd))
		{
			flags |= R_OK;
		}
		if (bit.test(wr))
		{
			flags |= W_OK;
		}
		if (bit.test(ex))
		{
			flags |= X_OK;
		}

		const auto str = path .data();
		return sys::access(str, flags);
	}

	span where(view hint)
	{
		if (hint == "run")
		{
			return env::usr::runtime();
		}
		else
		if (hint == "data")
		{
			return env::usr::data();
		}
		else
		if (hint == "cache")
		{
			return env::usr::cache();
		}
		else
		if (hint == "config")
		{
			return env::usr::config();
		}
		else
		{
			return env::path();
		}
	}

	string find(view name, view mode, view hint)
	{
		const auto dirs = where(hint);
		for (auto base : dirs)
		{
			if (env::file::any_of(base, mask(mode)))
			{
				return fmt::path::join(base, name);
			}
		}
		return { };
	}

	unique_ptr enclose(basic_ptr f)
	{
		return fwd::make_unique<FILE>(f, [](auto f)
		{
			if (nullptr != f)
			{
				if (EOF == std::fclose(f))
				{
					perror("fclose");
				}
			}
		});
	}

	unique_ptr list(view path)
	{
		auto const pair = pipe();
		auto buf = fmt::to_string(path);
		sys::thread([=]
		{
			auto f = enclose(pair.second);
			auto s = buf.c_str();
			std::setbuf(pair.second, nullptr);
			for (auto it : sys::files(s))
			{
				std::fputs(it, pair.second);
			}
		});
		return enclose(pair.first);
	}

	unique_ptr open(view path, view mode, size_t sz)
	{
		if (not fmt::terminated(path))
		{
			auto buf = fmt::to_string(path);
			return open(buf, mode, sz);
		}

		if (not fmt::terminated(mode))
		{
			auto buf = fmt::to_string(mode);
			return open(path, buf, sz);
		}

		auto f = std::fopen(path.data(), mode.data());
		if (nullptr == f)
		{
			#ifdef perror
			perror("fopen", mode, path);
			#else
			perror("fopen");
			#endif
		}
		return enclose(f);
	}

	unique_ptr lock(basic_ptr f, view mode, off_t off, size_t sz)
	{
		#ifdef assert
		assert(nullptr != f);
		#endif

		const auto fd = sys::fileno(f);
		if (sys::fail(fd))
		{
			perror("fileno");
		}

		#ifdef _WIN32
		{
			const auto h = sys::win::get(fd);
			if (sys::win::fail(h))
			{
				perror("_get_osfhandler");
			}

			if (0 == sz)
			{
				sz = SIZE_MAX;
			}

			const auto mask = to_flags(mode);

			DWORD dw = 0;
			if (mask & O_EXCL)
			{
				dw |= LOCKFILE_EXCLUSIVE_LOCK;
			}
			if (mask & O_CREAT)
			{
				dw |= LOCKFILE_FAIL_IMMEDIATELY;
			}

			sys::win::overlapped over = off;
			sys::win::large_int large = sz;

			if (not LockFileEx(h, dw, 0, large.low_part(), large.high_part(), &over))
			{
				#ifdef WINERR
				WINERR("LockFileEx");
				#endif
			}
			else return fwd::make_unique<FILE>(f, [=](basic_ptr)
			{
				if (not UnlockFileEx(h, 0, large.low_part(), large.high_part(), &over))
				{
					#ifdef WINERR
					WINERR("UnlockFileEx");
					#endif
				}
			});
		}
		#else
		{
			sys::uni::lock key;
			key.l_whence = SEEK_SET;
			key.l_start = off;
			key.l_len = sz;

			bool wr = fwd::any_of(mode, fwd::equal_to('w'));
			bool cr = fwd::any_of(mode, fwd::equal_to('c'));

			if (wr)
			{
				key.l_type = F_WRLCK;
			}
			else
			{
				key.l_type = F_RDLCK;
			}

			if (cr ? key.set(fd) : key.wait(fd))
			{
				perror(wr ? "F_WRLCK" : "F_RDLCK");
			}
			else return fwd::make_unique<FILE>(f, [=](basic_ptr)
			{
				sys::uni::lock key;
				key.l_type = F_UNLCK;
				key.l_whence = SEEK_SET;
				key.l_start = off;
				key.l_len = sz;
				if (key.set(fd))
				{
					perror("F_UNLCK");
				}
			});
		}
		#endif

		return nullptr;
	}

	unique_buf map(basic_ptr f, view mode, off_t off, size_t sz, basic_buf buf)
	{
		int fd = sys::fileno(f);
		if (sys::fail(fd))
		{
			perror("fileno");
		}

		if (0 == sz)
		{
			sys::stat_t st[1];
			if (sys::fstat(fd, st))
			{
				perror("fstat");
			}
			else sz = st->st_size;
		}

		#ifdef _WIN32
		{
			const auto h = sys::win::get(fd);
			#ifdef assert
			assert(not sys::win::fail(h));
			#endif

			DWORD flags = 0;
			DWORD prot = 0;

			if (mask & xu)
			{
				flags |= FILE_MAP_COPY;
			}

			if (mask & ex)
			{
				flags |= FILE_MAP_EXECUTE;

				if ((mask & rw) == rw)
				{
					prot = PAGE_EXECUTE_READWRITE;
					flags |= FILE_MAP_ALL_ACCESS;
				}
				else
				if (mask & wr)
				{
					prot = (mask & xu) ? PAGE_EXECUTE_WRITECOPY : PAGE_EXECUTE_READWRITE;
					flags |= FILE_MAP_WRITE;
				}
				else
				if (mask & rd)
				{
					prot = PAGE_EXECUTE_READ;
					flags |= FILE_MAP_READ;
				}
			}
			else
			{
				if ((mask & rw) == rw)
				{
					prot = PAGE_READWRITE;
					flags |= FILE_MAP_ALL_ACCESS;
				}
				else
				if (mask & wr)
				{
					prot = (mask & xu) ? PAGE_WRITECOPY : PAGE_READWRITE;
					flags |= FILE_MAP_WRITE;
				}
				else
				if (mask & rd)
				{
					prot = PAGE_READONLY;
					flags |= FILE_MAP_READ;
				}
			}

			sys::win::handle const hm = CreateFileMapping
			(
				h, nullptr, prot, HIWORD(sz), LOWORD(sz), nullptr
			);

			if (sys::win::fail(hm))
			{
				#ifdef WINERR
				WINERR("CreateFileMapping");
				#endif
			}

			return sys::win::mem::map<char>(hm, flags, off, sz, buf);
		}
		#else
		{
			int prot = 0;
			int flags = 0;

			if (fmt::any_of(mode, 'r'))
			{
				prot |= PROT_READ;
			}
			if (fmt::any_of(mode, 'w'))
			{
				prot |= PROT_WRITE;
			}
			if (fmt::any_of(mode, 'x'))
			{
				prot |= PROT_EXEC;
			}

			if (fmt::any_of(mode, 'c'))
			{
				flags |= MAP_PRIVATE;
			}
			else
			{
				flags |= MAP_SHARED;
			}

			return sys::uni::shm::map(sz, prot, flags, fd, off, buf);
		}
		#endif
	}

	basic_pair pipe([[maybe_unused]] size_t sz)
	{
		int fd[2] = { sys::invalid };

	#ifdef _WIN32

		if (HANDLE h[2]; CreatePipe(h + 0, h + 1, nullptr, (DWORD) sz))
		{
			for (int n : { 0, 1 })
			{
				fd[n]= sys::win::open(h[n], n ? O_WRONLY : O_RDONLY);
				if (sys::fail(fd[n]))
				{
					#ifdef perror
					perror("_open_osfhandler", n);
					#else
					perror("_open_osfhandler");
					#endif
				}
			}
		}
		else
		{
			#ifdef WINERR
			WINERR("CreatePipe");
			#endif
			return { };
		}

	#else

		if (sys::pipe(fd))
		{
			perror("pipe");
			return { };
		}

	#endif

		basic_ptr f[2] = { nullptr };

		for (int n : { 0, 1 })
		{
			f[n] = sys::fdopen(fd[n], n ? "r" : "w");
			if (nullptr == f[n])
			{
				#ifdef perror
				perror("fdopen", n);
				#else
				perror("fdopen");
				#endif
			}
		}

		return { f[0], f[1] };
	}

	unique_ptr pipe(view path, view mode, size_t sz)
	{
		if (path.size() > path.find(sys::tag::dir))
		{
			#ifdef WARN
			WARN("Invalid", path);
			#endif
			return { };
		}

		if (not terminated(path))
		{
			auto buf = fmt::to_string(path);
			return pipe(buf, mode, sz);
		}

	#ifdef _WIN32

		const bool input = fwd::any_of(mode, 'r');
		const auto dw = input ? PIPE_ACCESS_INBOUND : PIPE_ACCESS_OUTBOUND;

		sys::win::handle h = CreateNamedPipe
		(
			path.data(), dw, 0, 1, 0, 0, 0, nullptr
		);

		if (sys::win::fail(h))
		{
			#ifdef WINERR
			WINERR("CreateNamedPipe", path);
			#endif
			return { };
		}

		auto [read, write] = pipe(sz);

		sys::thread([=]
		{
			auto f = enclose(input ? write : read);

			if (ConnectNamedPipe(h.get(), nullptr))
			{
				char buf[BUFSIZ];

				if (input)
				{
					do if (DWORD n; ReadFile(h.get(), buf, sizeof buf, &n, nullptr))
					{
						std::size_t sz;
						for (DWORD m = 0; m < n; m += sz)
						{
							sz = std::fwrite(buf + m, 1, n - m, f.get());
							if (not sz) break;
						}

						if (std::ferror(f.get()))
						{
							perror("fwrite");
						}
						else
						if (std::fflush(f.get()))
						{
							perror("fflush");
						}
					}
					else
					{
						#ifdef WINERR
						WINERR("ReadFile");
						#endif
					}
					while (not std::feof(f.get()));
				}
				else
				{
					do if (auto sz = std::fread(buf, 1, sizeof buf, f.get()))
					{
						for (DWORD n, m = 0; m < sz; m += n)
						{
							if (not WriteFile(h.get(), buf + m, sz - m, &n, nullptr))
							{
								#ifdef WINERR
								WINERR("WriteFile");
								#endif
								break;
							}
						}

						if (not FlushFileBuffers(h.get()))
						{
							#ifdef WINERR
							WINERR("FlushFileBuffers");
							#endif
						}
					}
					while (not std::feof(f.get()));
				}

				if (DisconnectNamedPipe(h.get()))
				{
					#ifdef WINERR
					WINERR("DisconnectNamedPipe");
					#endif
				}
			}
			else
			{
				#ifdef WINERR
				WINERR("ConnectNamedPipe");
				#endif
			}
		});

		return enclose(input ? read : write);

	#else

		const auto flags = to_mode(mode);
		if (mkfifo(path.data(), flags))
		{
			#ifdef perror
			perror("mkfifo", path);
			#else
			perror("mkfifo");
			#endif
			return { };
		}
		return open(path, mode, sz);

	#endif

	}

	string name(basic_ptr f)
	{
		string buf;

		#ifdef assert
		assert(nullptr != f);
		#endif

		const int fd = sys::fileno(f);
		if (sys::fail(fd))
		{
			perror("fileno");
		}

	#ifdef _WIN32

		const auto h = sys::win::get(fd);

		#ifdef assert
		assert(not sys::win::fail(h));
		#endif

		union
		{
			FILE_NAME_INFO info;
			char data[sizeof info + MAX_PATH];
		};

		if (GetFileInformationByHandleEx(h, FileNameInfo, data, sizeof data))
		{
			auto w = fmt::wide(info.FileName, info.FileNameLength);
			buf = fmt::to_string(w);
		}
		else
		{
			#ifdef WINERR
			WINERR("GetFileInformationByHandleEx", fd);
			#endif
		}

	#elif defined(F_GETPATH)

		buf.resize(MAXPATHLEN);
		if (sys::fail(fcntl(fd, F_GETPATH, buf.data())))
		{
			perror("F_GETPATH");
			buf.clear();
		}
		else
		{
			const auto pos = buf.find_last_of("\0");
			if (fmt::npos != pos) buf.resize(pos);
		}

	#elif defined(__bsdi__) || defined(__linux__)

		using namespace std::literals;
		static const auto pid = sys::getpid();
		const auto link = "/proc/"s + fmt::to_string(pid) + "/fd/" + fmt::to_string(fd);

		buf.resize(PATH_MAX);
		const auto sz = readlink(link.c_str(), buf.data(), buf.size());
		if (sys::fail(sz))
		{
			perror("readlink");
			buf.clear();
		}
		else
		{
			buf.resize(sz);
		}

	#endif

		return buf;
	}
}

#ifdef TEST
TEST(to_flags)
{
	using namespace env::file;
	ASSERT(to_flags("r") == (O_RDONLY));
	ASSERT(to_flags("w") == (O_WRONLY | O_CREAT | O_TRUNC));
	ASSERT(to_flags("a") == (O_WRONLY | O_CREAT | O_APPEND));
	ASSERT(to_flags("r+") == (O_RDWR));
	ASSERT(to_flags("w+") == (O_RDWR | O_CREAT | O_TRUNC));
	ASSERT(to_flags("a+") == (O_RDWR | O_CREAT | O_APPEND));
}
TEST(from_flags)
{
	using namespace env::file;
	ASSERT(from_flags(O_RDONLY) == "r");
	ASSERT(from_flags(O_RDWR) == "r+");
}
TEST(to_mode)
{
	using namespace env::file;
	ASSERT(to_mode("u=rwx") == S_IRWXU);
	ASSERT(to_mode("g+rw,o=r") == (S_IRGRP | S_IWGRP | S_IROTH));
	ASSERT(to_mode("u-x", S_IRWXU) == (S_IRUSR | S_IWUSR));
	ASSERT(to_mode("u+x", S_IRWXG) == (S_IRWXG | S_IXUSR));
	ASSERT(to_mode("u-rw", S_IRWXU) == S_IXUSR);
	ASSERT(to_mode("u+rw", S_IXUSR) == S_IRWXU);
}
TEST(from_mode)
{
	using namespace env::file;
	ASSERT(from_mode(S_IRWXU) == "rwx------");
	ASSERT(from_mode(S_IRWXG) == "---rwx---");
	ASSERT(from_mode(S_IRWXO) == "------rwx");
	ASSERT(from_mode(S_IRUSR|S_IRGRP|S_IROTH) == "r--r--r--");
	ASSERT(from_mode(S_IWUSR|S_IWGRP|S_IWOTH) == "-w--w--w-");
	ASSERT(from_mode(S_IXUSR|S_IXGRP|S_IXOTH) == "--x--x--x");
}
TEST(lock)
{
	using namespace env::file;
	auto f = temp();
	ASSERT(f and "Cannot open file");
	auto read = lock(f.get(), "r");
	ASSERT(read and "Cannot lock file to read");
	auto write = lock(f.get(), "w");
	ASSERT(not write and "Can lock file to write");
}
TEST(pipe)
{
	auto [read, write] = env::file::pipe();

	ASSERT(read);
	ASSERT(write);

	constexpr auto str = "Mario";
	if (std::fputs(str, write))
	{
		perror("fputs");
	}

	char buf[sizeof str];
	if (std::fgets(buf, sizeof buf, read) == nullptr)
	{
		perror("fgets");
	}

	ASSERT(std::strcmp(str, buf) == 0);
}
#endif
