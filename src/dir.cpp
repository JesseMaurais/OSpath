// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "err.hpp"
#include "dir.hpp"
#include "env.hpp"
#include "usr.hpp"
#include "arg.hpp"
#include "sys.hpp"
#include "dig.hpp"
#include "file.hpp"
#include "sync.hpp"
#include "type.hpp"
#include "algo.hpp"
#include <regex>
#include <stack>

#ifdef _WIN32
#include "win/file.hpp"
#else
#include "uni/dirent.hpp"
#include "uni/fcntl.hpp"
#endif

namespace fmt::path
{
	vector split(view line)
	{
		return fmt::split(line, sys::tag::path);
	}

	string join(span part)
	{
		return fmt::join(part, sys::tag::path);
	}

	string join(init list)
	{
		return join(fwd::to_span(list));
	}
}

namespace fmt::dir
{
	vector split(view line)
	{
		return fmt::split(line, sys::tag::dir);
	}

	string join(span part)
	{
		return fmt::join(part, sys::tag::dir);
	}

	string join(init list)
	{
		return join(fwd::to_span(list));
	}
}

namespace fmt::file
{
	vector split(view line)
	{
		return fmt::split(line, fmt::tag::dot);
	}

	string join(span part)
	{
		return fmt::join(part, fmt::tag::dot);
	}

	string join(init list)
	{
		return join(fwd::to_span(list));
	}
}

namespace env::file
{
	bool any_of(view paths, test pass)
	{
		if (not terminated(paths))
		{
			const auto buf = to_string(paths);
			return env::file::any_of(buf, pass);
		}
	
		for (auto path : fmt::path::split(paths))
		{
			auto range = sys::files(path.data());
			if (fwd::any_of(range, pass))
			{
				return success;
			}
		}
		return failure;
	}

	bool any_of(span paths, test pass)
	{
		return fwd::any_of(paths, [pass](auto path)
		{
			return env::file::any_of(path, pass);
		});
	}

	bool any_of(init paths, test pass)
	{
		auto extent = fwd::to_span(paths);
		return any_of(extent, pass);
	}

	test mask(view mode)
	{
		const auto buf = to_string(mode);
		return [=](view path)
		{
			return not env::file::access(path, buf);
		};
	}

	test regex(view pattern)
	{
		const auto buf = to_string(pattern);
		const auto rgx = std::regex(buf);
		return [rgx](view path)
		{
			std::cmatch match;
			const auto buf = to_string(path);
			return std::regex_search(buf.data(), match, rgx);
		};
	}

	test copy(cache& buf)
	{
		return [&](view path)
		{
			buf.emplace(path.data(), path.size());
			return success;
		};
	}

	test copy(string& buf)
	{
		return [&](view path)
		{
			buf = to_string(path);
			return success;
		};
	}
}

#ifdef TEST
TEST(dir)
{
	ASSERT(not env::file::access(env::temp()));
	ASSERT(not env::file::access(env::pwd()));
	ASSERT(not env::file::access(env::home()));

	const auto path = fmt::dir::split(__FILE__);
	ASSERT(not path.empty());
	const auto name = path.back();
	ASSERT(not name.empty());
	const auto program = env::opt::program();
	ASSERT(not program.empty());

	ASSERT(env::file::any_of(env::pwd(), [program](auto entry)
	{
		return fmt::dir::split(entry).back() == program;
	}));

	const auto temp = fmt::dir::join(env::temp(), "my", "test", "dir");
	if (temp.empty()) return;
//	const auto stem = env::file::mkdir(temp);
//	ASSERT(not empty(stem.first));
//	ASSERT(not empty(stem.second));
//	ASSERT(not env::file::rmdir(stem));
}
TEST(ext)
{
	fmt::view dir, name, path = __FILE__;
	auto pos = path.find_last_of(sys::tag::dir);
	if (auto pair = path.split(pos); pair.second.empty())
	{
		dir = ".";
		name = pair.first;
	}
	else
	{
		dir = pair.first;
		name = pair.second;
	}
}
#endif
