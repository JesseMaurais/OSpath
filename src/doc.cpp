// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "err.hpp"
#include "doc.hpp"
#include "meta.hpp"

#ifdef _WIN32
#include "win.hpp"
#else
#include "uni.hpp"
#endif

namespace doc
{
	template class instance<fwd::event>;

	sys::exclusive<std::unordered_map<std::type_index, fwd::as_ptr<interface>>> registry;

	interface* find(std::type_index index)
	{
		auto data = registry.reader();
		auto it = data->find(index);
		return data->end() == it
			? nullptr : it->second;
	}
}

#ifdef TEST
namespace
{
	struct dumb
	{
		int i = 0;
		char b = 0;
		short w = 0;
		long n = 0;
		float f = 0.0f;
		double d = 0.0;
		fmt::string s = "Hello World";

		static constexpr std::tuple table
		{
			&dumb::i,
			&dumb::f,
			&dumb::s,
		};
	};
}

template class doc::instance<dumb>;
template <> fmt::view doc::name<&dumb::i> = "i";
template <> fmt::view doc::name<&dumb::f> = "f";
template <> fmt::view doc::name<&dumb::s> = "s";

TEST(doc)
{
	using parent_type = fwd::offset_of<&dumb::i>::parent_type;
	static_assert(std::is_same<parent_type, dumb>::value);

	using value_type = fwd::offset_of<&dumb::i>::value_type;
	static_assert(std::is_same<value_type, int>::value);

	static auto dummy = doc::instance<dumb>::address();
	ASSERT(0 == dummy->size());
	const int id = dummy->emplace({});
	ASSERT(1 == dummy->size());
	ASSERT(0 == id);

	ASSERT(dummy->contains(id));
	auto data = dummy->writer(id);
	ASSERT(data);
	auto ptr = data.get();
	ASSERT(nullptr != ptr);

	ASSERT(doc::key<0>(ptr) == "i");
	ASSERT(doc::key<1>(ptr) == "f");
	ASSERT(doc::key<2>(ptr) == "s");

	ASSERT(doc::get<0>(ptr) == 0);
	ASSERT(doc::get<1>(ptr) == 0.0f);
	ASSERT(doc::get<2>(ptr) == "Hello World");

	ASSERT(doc::get<0>(ptr) == ptr->i);
	ASSERT(doc::get<1>(ptr) == ptr->f);
	ASSERT(doc::get<2>(ptr) == ptr->s);

	doc::set<0>(ptr) = 42;
	doc::set<1>(ptr) = 4.2f;
	doc::set<2>(ptr) = "42";
	
	fmt::buffer buf;
	doc::for_each(ptr, [&]<size_t N>()
	{
		buf << doc::key<N>(ptr) << "=" << doc::get<N>(ptr) << "\n";
	});
	auto str = buf.str();
	ASSERT(not str.empty());

	ASSERT(doc::get<0>(ptr) == 42);
	ASSERT(doc::get<1>(ptr) == 4.2f);
	ASSERT(doc::get<2>(ptr) == "42");

	ASSERT(doc::get<0>(ptr) == ptr->i);
	ASSERT(doc::get<1>(ptr) == ptr->f);
	ASSERT(doc::get<2>(ptr) == ptr->s);

	ASSERT(1 == dummy->size());
	dummy->destroy(id);
	ASSERT(0 == dummy->size());
}
#endif
