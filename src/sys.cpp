// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "err.hpp"
#include "sys.hpp"
#include "ptr.hpp"
#include "sync.hpp"

namespace sys
{
	thread_local std::stringstream local_buf;

	std::ostream& out()
	{
		return local_buf;
	}

	std::ostream& put(std::ostream& buf)
	{
		std::string line;
		while (std::getline(local_buf, line))
		{
			buf << line << std::endl;
		}
		return buf;
	}

	const char* strerr(int no)
	{
		static thread_local char buf[BUFSIZ];
		#ifdef __STDC_LIB_EXT1__
		{
			if (no = strerror_s(buf, sizeof buf, no))
			{
				#ifdef ERR
				ERR(no, "strerror_s");
				#endif
			}
			return buf;
		}
		#elif 0 < _POSIX_C_SOURCE
		{
			#ifdef _GNU_SOURCE
			{
				return strerror_r(no, buf, sizeof buf);
			}
			#else
			{
				auto tmp = errno;
				if (no = strerror_r(no, buf, sizeof buf))
				{
					constexpr auto prefix = "strerror_r";
					if (sys::fail(no))
					{
						perror(prefix);
					}
					else
					{
						#ifdef ERR
						ERR(no, prefix);
						#endif
					}
				}
				errno = tmp;
				return buf;
			}
			#endif
		}
		#else
		{
			return std::strncpy(buf, std::strerror(no), sizeof buf);
		}
		#endif
	}
	
	void psignal(int no, const char* prefix)
	{
		#if _POSIX_C_SOURCE >= 200809L
		{
			::psignal(no, prefix);
		}
		#else
		{
			::fprintf(stderr, "%s: %s\n", prefix, sys::strsignal(no));
		}
		#endif
	}
	
	const char* strsignal(int no)
	{
		#if _POSIX_C_SOURCE >= 200809L
		{
			return ::strsignal(no);
		}
		#else
		{
			switch (no)
			{
				default:
				{
					static char buf[64];
					sprintf(buf, sizeof buf, "Unknown signal #%d", no);
					return buf;
				}
			
				#ifdef SIGABRT
				case SIGABRT: return "Process abort signal.";
				#endif
				
				#ifdef SIGALRM
				case SIGALRM: return "Alarm clock.";
				#endif
				
				#ifdef SIGBUS
				case SIGBUS: return "Access to an undefined portion of a memory object.";
				#endif
				
				#ifdef SIGCHLD
				case SIGCHLD: return "Child process terminated, stopped, or continued.";
				#endif
				
				#ifdef SIGCONT
				case SIGCONT: return "Continue executing, if stopped.";
				#endif
				
				#ifdef SIGFPE
				case SIGFPE: return "Erroneous arithmetic operation.";
				#endif
				
				#ifdef SIGHUP
				case SIGHUP: return "Hang up.";
				#endif
				
				#ifdef SIGILL
				case SIGILL: return "Illegal instruction.";
				#endif
				
				#ifdef SIGINT
				case SIGINT: return "Terminal interrupt signal.";
				#endif
				
				#ifdef SIGKILL
				case SIGKILL: return "Kill (cannot be caught or ignored).";
				#endif
				
				#ifdef SIGPIPE
				case SIGPIPE: return "Write on a pipe with no one to read it.";
				#endif
				
				#ifdef SIGPOLL
				case SIGPOLL: return "Pollable event.";
				#endif
				
				#ifdef SIGQUIT
				case SIGQUIT: return "Terminal quit signal.";
				#endif
				
				#ifdef SIGSEGV
				case SIGSEGV: return "Invalid memory reference.";
				#endif
				
				#ifdef SIGSTOP
				case SIGSTOP: return "Stop executing (cannot be caught or ignored).";
				#endif
				
				#ifdef SIGSYS
				case SIGSYS: return "Bad system call.";
				#endif
				
				#ifdef SIGTERM
				case SIGTERM: return "Termination signal.";
				#endif
				
				#ifdef SIGTSTP
				case SIGTSTP: return "Terminal stop signal.";
				#endif
				
				#ifdef SIGTTIN
				case SIGTTIN: return "Background process attempting read.";
				#endif
				
				#ifdef SIGTTOU
				case SIGTTOU: return "Background process attempting write.";
				#endif
							
				#ifdef SIGTRAP
				case SIGTRAP: return "Trace/breakpoint trap.";
				#endif
				
				#ifdef SIGURG
				case SIGURG: return "High bandwidth data is available at a socket.";
				#endif
				
				#ifdef SIGUSR1
				case SIGUSR1: return "User-defined signal 1.";
				#endif
				
				#ifdef SIGUSR2
				case SIGUSR2: return "User-defined signal 2.";
				#endif
				
				#ifdef SIGPROF
				case SIGPROF: return "Profiling timer expired.";
				#endif
				
				#ifdef SIGVTALRM
				case SIGVTALRM: return "Virtual timer expired.";
				#endif
				
				#ifdef SIGXCPU
				case SIGXCPU: return "CPU time limit exceeded.";
				#endif
				
				#ifdef SIGXFSZ
				case SIGXFSZ: return "File size limit exceeded. ";
				#endif
			};
		}
		#endif
	}

	pid_t exec(int fd[3], int argc, char** argv)
	{
		#ifdef assert
		assert(nullptr != argv);
		assert(nullptr != argv[0]);
		assert(nullptr == argv[argc]);
		#endif

		for (int n : { 0, 1, 2 })
		{
			fd[n] = invalid;
		}

		#ifdef _WIN32
		{
			win::handle pair[3][2];
			win::attributes attr;

			for (int n : { 0, 1, 2 })
			{
				if (HANDLE h[2]; not attr.pipe(h))
				{
					#ifdef WINERR
					WINERR("CreatePipe");
					#endif
					return invalid;
				}
				else
				{
					for (int m : { 0, 1 })
					{
						pair[n][m] = h[m];
					}
					
					if (not SetHandleInformation(h[not n], HANDLE_FLAG_INHERIT, FALSE))
					{
						#ifdef WINERR
						WINERR("SetHandleInformation");
						#endif
						return invalid;
					}
				}
			}

			char cmd[MAX_PATH];
			ZeroMemory(cmd, sizeof cmd);
			for (size_t i = 0, j = 0; argv[i]; ++i, ++j)
			{
				constexpr auto max = sizeof cmd;
				const auto format = fmt::any_of(argv[i]) ? "\"%s\" " : "%s ";
				const auto n = std::snprintf(cmd + j, max - j, format, argv[i]);
				if (0 < n) j += n - 1;
				else return invalid;
			}

			win::zero<PROCESS_INFORMATION> pi;
			win::size<&STARTUPINFO::cb> si;

			si.dwFlags = STARTF_USESTDHANDLES;
			si.hStdInput = pair[0][0];
			si.hStdOutput = pair[1][1];
			si.hStdError = pair[2][1];

			const auto ok = attr.process
			(
				nullptr,          // application
				cmd,              // command line
				DETACHED_PROCESS, // creation flags
				nullptr,          // environment
				nullptr,          // current directory
				&si,              // start-up info
				&pi               // process info
			);

			if (not ok)
			{
				#ifdef WINERR
				WINERR("CreateProcess", cmd);
				#endif
				return invalid;
			}

			sys::win::handle::enclose(pi.hThread);

			for (int n : { 0, 1, 2 })
			{
				fd[n] = n
					? sys::win::open(pair[n][0], O_RDONLY)
					: sys::win::open(pair[n][1], O_WRONLY);
			}

			return pi.dwProcessId;
		}
		#else
		{
			uni::filed pair[3][2];

			for (int n : { 0, 1, 2 })
			{
				if (int fd[2]; fail(pipe(fd)))
				{
					perror("pipe");
					return invalid;
				}
				else for (int m : { 0, 1 })
				{
					pair[n][m] = fd[m];
				}
			}

			if (auto pid = fork(); pid)
			{
				if (fail(pid))
				{
					perror("fork");
				}
				else for (int n : { 0, 1, 2 })
				{
					fd[n] = dup(pair[n][not n]);
				}
				return pid;
			}

			for (int n : { 0, 1, 2 })
			{
				if (fail(close(n)))
				{
					perror("close");
				}

				if (fail(dup2(pair[n][not n], n)))
				{
					perror("dup2");
				}

				for (int fd : pair[n])
				{
					if (fail(close(fd)))
					{
						perror("close");
					}
				}
			}

			auto code = execvp(argv[0], argv);
			perror("execvp");
			std::exit(code);
		}
		#endif
	}
}

#ifdef _WIN32
namespace sys::win
{
	const char* strerr(unsigned long id, void* ptr)
	{
		constexpr auto flag = FORMAT_MESSAGE_ALLOCATE_BUFFER
		                    | FORMAT_MESSAGE_IGNORE_INSERTS
		                    | FORMAT_MESSAGE_FROM_HMODULE
		                    | FORMAT_MESSAGE_FROM_SYSTEM;

		auto h = static_cast<HANDLE>(ptr);
		if (sys::win::fail(h))
		{
			h = GetModuleHandle(nullptr);
		}

		static thread_local local tls;

		LPSTR str = nullptr;
		auto data = (LPSTR) std::addressof(str);
		const auto size = FormatMessage
		(
			flag,   // style
			h,      // module
			id,     // message
			0,      // language
			data,   // buffer
			0,      // size
			nullptr // arguments
		);

		if (0 < size) // replace
		{
			tls = str;
		}

		if (nullptr == str)
		{
			static thread_local char buf[128] = { };
			std::sprintf(buf, "Unknown error %lu", id);
			str = buf;
		}

		return str;
	}
	
	local sddl(LPSTR str)
	{
		PISECURITY_DESCRIPTOR h = nullptr
		if (str != nullptr)
		{
			if (not ConvertStringSecurityDescriptorToSecurityDescriptor(str, SDDL_REVISION_1, &h, NULL))
			{
				#ifdef WINERR
				WINERR("ConvertStringSecurityDescriptorToSecurityDescriptor", str);
				#endif
			}
		}
		return h;
	}
}
#endif
