#ifndef arg_hpp
#define arg_hpp "Command Line Arguments"

#include "opt.hpp"

namespace fmt::opt
{
	constexpr pair posix { "--", "=" };
	constexpr pair win32 { "/", ":" };

	using pair = table::type;
	using span = table::span;
	using init = table::init;

	string join(pair, pair=posix);
	string join(span, pair=posix);
	string join(init, pair=posix);

	template <class... View> auto join(view u, View... w)
	{
		return join({ u , w... });
	}
}

namespace env::opt
{
	using namespace fmt;

	view application();
	span arguments();
	view program();
	view config();

	bool got(view);
	view get(view);
	bool set(view, view);

	bool got(pair);
	view get(pair);
	bool set(pair, view);

	struct cmd : layout<cmd>
	{
		long argn; // required arguments (or -1 for any number)
		view dash; // short name with one dash (-c)
		view name; // long name with dual dash (--config)
		view text; // descriptive text for user help menu
	};

	vector parse(int argc, char** argv, cmd::span);
	// Put command line arguments into options
	output set(output);
	// Write to stream
	input get(input);
	// Read from stream
};

#endif // file
