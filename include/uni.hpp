#ifndef uni_hpp
#define uni_hpp "POSIX Utility"

#ifdef _WIN32
# error POSIX utility header included with WIN32 macro.
#endif

#include "sys.hpp"
#include "ptr.hpp"

namespace sys::uni
{
	struct filed : fwd::source<int>
	{
		static auto close(int fd)
		{
			return fwd::shared_ptr<int>(nullptr, [fd](auto ptr) noexcept
			{
				if (not fail(fd) and fail(::close(fd)))
				{
					perror("close");
				}
				#ifdef assert
				assert(nullptr == ptr);
				#endif
			});
		}
		
		filed(int fd = invalid)
		: source(fd, close(fd))
		{ }
	};
}

#endif // file
