#ifndef cmd_hpp
#define cmd_hpp "Command Line"

#include "file.hpp"

namespace env::cmd
{
	using namespace file;

	struct iterator
	{
		string buf;
		unique_ptr ptr;
		bool operator!=(as_view<iterator>) const;
		view operator*() const;
		void operator++();
	};

	using range = fwd::range<iterator>;

	range get(view, size_t = 0);
	range get(span, size_t = 0);
	range get(init, size_t = 0);

	template <class... View> auto get(view u, View... w)
	{
		return get({ u, w... });
	}

	range echo(view line);
	range start(view path);
	range read(view path);
	range list(view dir = {});
	range find(view dir = {});
	range which(view program);
	range imports(view path);
	range exports(view path);

	bool desktop(view name);
	range notify(view text, view icon = {});
	range calendar(view text = {}, view format = {}, int day = 0, int month = 0, int year = 0);
	range color(view start = {}, bool palette = true);
	range enter(view start, view label = {}, bool hide = false);
	range show(view text, view type = {});
	range text(view path, view check = {}, view font = {}, view type = {});
	range select(view start = {}, view mode = {});
}

#endif // file
