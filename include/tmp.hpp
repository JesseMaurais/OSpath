#ifndef tmp_hpp
#define tmp_hpp "Template Functions"

#include "fwd.hpp"

namespace fwd
{
	template <class T> using function = std::function<T(T)>;
	template <class T> using put = std::function<void(T)>;
	template <class T> using get = std::function<T()>;

	template <class It> struct range
	{
		get<It> begin, end;
	};
	
	template <class It> range<It> make_range(It begin, It end)
	{
		return { [=] { return begin; }, [=] { return end; } };
	}

	using event = get<void>;

	struct end : private event
	{
		using event::event;

		~end()
		{
			event::operator()();
		}
	};

	template <class... Q> constexpr auto always = [](Q...) { return true; };
	template <class... Q> constexpr auto never = [](Q...) { return false; };

	template <class... T> struct formula : std::function<bool(T...)>
	{
		using base = std::function<bool(T...)>;
		using base::base;

		formula operator and(as_view<base> that) const
		{
			return [&](T... x)
			{
				return (*this)(x...) and that(x...);
			};
		}

		formula operator or(as_view<base> that) const
		{
			return [&](T... x)
			{
				return (*this)(x...) or that(x...);
			};
		}

		formula operator xor(as_view<base> that) const
		{
			return [&](T... x)
			{
				return (*this)(x...) xor that(x...);
			};
		}

		formula operator not() const
		{
			return [&](T... x)
			{
				return not (*this)(x...);
			};
		}
	};

	template <class T> using predicate = formula<T>;

	template <class T, class S=T> using relation = formula<T, S>;

	template <class T> predicate<T> equal_to(T v)
	{
		return [=](T u) { return u == v; };
	}

	template <class T> predicate<T> less_than(T v)
	{
		return [=](T u) { return u < v; };
	}

	template <class T> predicate<T> greater_than(T v)
	{
		return [=](T u) { return u > v; };
	}
}

#endif // file
