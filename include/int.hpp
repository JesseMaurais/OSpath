#ifndef int_hpp
#define int_hpp "Intervals"

namespace fwd
{
	template <auto First, auto Last> struct interval
	{
		constexpr auto begin() const
		{
			return First;
		}

		constexpr auto end() const
		{
			return Last + 1;
		}

		constexpr auto size() const
		{
			return end() - begin();
		}

		template <class N> constexpr bool greater(N n) const
		{
			return Last < n;
		}

		template <class N> constexpr bool less(N n) const
		{
			return n < First;
		}

		template <class N> constexpr bool contains(N n) const
		{
			return not greater(n) and not less(n);
		}
	};

	template <class Part, class... Parts> struct interval_union
	{
		using Rest = interval_union<Parts...>;

		constexpr auto begin() const
		{
			return Part::begin() < Rest::begin() ? Part::begin() : Rest::begin();
		}

		constexpr auto end() const
		{
			return Part::end() < Rest::end() ? Rest::end() : Part::end();
		}

		constexpr auto size() const
		{
			return end() - begin();
		}
		
		template <class N> constexpr bool contains(N n) const
		{
			return Part::contains(n) or Rest::contains(n);
		}
	};

	template <class Part> struct interval_union<Part>
	{
		constexpr auto begin() const
		{
			return Part::begin();
		}

		constexpr auto end() const
		{
			return Part::end();
		}

		constexpr auto size() const
		{
			return Part::size();
		}
		
		template <class N> constexpr bool contains(N n)
		{
			return Part::contains(n);
		}
	};
}

#endif // file
