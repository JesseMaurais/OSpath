#ifndef file_hpp
#define file_hpp "File System"

#include <cstdio>
#include "fmt.hpp"
#include "ptr.hpp"

namespace env::file
{
	using namespace fmt;
	
	using size_t = std::size_t;
	using off_t = std::ptrdiff_t;

	using basic_ptr = fwd::as_ptr<FILE>;
	using unique_ptr = fwd::unique_ptr<FILE>;
	using shared_ptr = fwd::shared_ptr<FILE>;
	using weak_ptr = std::weak_ptr<FILE>;

	using basic_buf = fwd::as_ptr<char>;
	using unique_buf = fwd::unique_ptr<char>;
	using shared_buf = fwd::shared_ptr<char>;
	using weak_buf = std::weak_ptr<char>;

	using basic_pair = fwd::pair<basic_ptr>;
	using unique_pair = fwd::pair<unique_ptr>;
	using shared_pair = fwd::pair<shared_ptr>;
	using weak_pair = fwd::pair<weak_ptr>;

	// Create a common pipe
	basic_pair pipe(size_t = 0);
	// Create a named pipe
	unique_ptr pipe(view, view = {}, size_t = 0);
	// Close the file when out of scope
	unique_ptr enclose(basic_ptr = nullptr);

	// List the files in a directory
	unique_ptr list(view);
	// Wait for events on file
	unique_ptr wait(view, view = {}, time_t = 0);
	// Open access to a given file
	unique_ptr open(view, view = {}, size_t = 0);
	// Change access to a file region
	unique_ptr lock(basic_ptr, view = {}, off_t = 0, size_t = 0);
	// Address a region of a file in shared memory
	unique_buf map(basic_ptr, view = {}, off_t = 0, size_t = 0, basic_buf = nullptr);

	// Path to file in system
	string name(basic_ptr);
	// Access to file
	bool access(view, view = "r");

	inline auto temp()
	{
		auto f = std::tmpfile();
		return enclose(f);
	}
}

#endif // file
