#ifndef fmt_hpp
#define fmt_hpp "Data Formats"

#include "fwd.hpp"
#include "tmp.hpp"
#include "ptr.hpp"

namespace fmt
{
	template <class Type> struct memory : fwd::type_of<Type>
	{
		using view = typename fwd::as_view<Type>;
		// address traits
		using ptr = typename std::add_pointer<Type>::type;
		using cptr = typename std::add_pointer<typename std::add_const<Type>::type>::type;
		using ref = typename std::add_lvalue_reference<Type>::type;
		using cref = typename std::add_lvalue_reference<typename std::add_const<Type>::type>::type;
		// access traits
		using access = fwd::function<ref>;
		using test = fwd::predicate<cref>;
		using compare = fwd::relation<cref>;
		using copy = fwd::predicate<ref>;
		using swap = fwd::relation<ref>;
	};

	template
	<
		class Char, template <class> class Traits = std::char_traits
	>
	struct stream
	{
		// address traits
		using addr = memory<Char>;
		using ios = memory<fwd::basic_ios<Char, Traits>>;
		using in = memory<fwd::basic_istream<Char, Traits>>;
		using out = memory<fwd::basic_ostream<Char, Traits>>;
		using io = memory<fwd::basic_iostream<Char, Traits>>;
		using buf = memory<fwd::basic_buf<Char, Traits>>;
		using file = memory<fwd::basic_file<Char, Traits>>;
		using str = memory<fwd::basic_stringstream<Char, Traits>>;
		using view = memory<fwd::basic_string_view<Char, Traits>>;

		// access traits
		using address = typename addr::cptr;
		using buffer = typename str::type;
		using test = typename view::test;
		using compare = typename view::compare;
		using binary = typename io::ref;
		using input = typename in::ref;
		using output = typename out::ref;
		using read = typename in::access;
		using write = typename out::access;
		using pipe = fwd::pair<read, write>;

		template <class Iterator>
		static output put(output out, Iterator begin, Iterator end, address tok)
		{
			auto it = std::ostream_iterator(out, tok);
			std::copy(begin, end, it);
			return out;
		}

		template <class Iterator>
		static output put(output out, Iterator begin, Iterator end, write tok)
		{
			for (auto it = begin; it != end; ++it)
				(it == begin ? out : out << tok) << *it;
			return out;
		}

		template <class Range, class Delimiter>
		static output put(output out, Range pair, Delimiter token)
		{
			return put(out, pair.begin(), pair.end(), token);
		}
	};

	template
	<
		class Type,
		template <class> class Alloc = std::allocator,
		template <class> class Order = std::less
	>
	struct layout
	{
		using type = Type;
		using pair = fwd::pair<Type>;
		using list = fwd::list<Type, Alloc>;
		using set = fwd::set<Type, Order, Alloc>;
		using map = fwd::map<Type, Type, Order, Alloc>;
		using init = fwd::init<Type>;
		using vector = fwd::vector<Type, Alloc>;
		using span = fwd::span<Type>;
	};

	template
	<
		class String,
		class Char,
		template <class> class Traits = std::char_traits,
		template <class> class Alloc = std::allocator,
		template <class> class Order = std::less,
		class Memory = memory<String>,
		class Stream = stream<Char, Traits>,
		class Layout = layout<String, Alloc, Order>
	>
	struct basic_string_type : String, Memory, Stream, Layout
	{
		using pipe = typename Stream::pipe;
		using stream = typename Stream::str;
		using test = typename Stream::test;
		using compare = typename Stream::compare;
		using iterator = typename String::iterator;
		using size_type = typename String::size_type;

		auto sub(iterator it, iterator end)
		{
			const auto begin = String::begin();
			const auto pos = std::distance(begin, it);
			const auto size = std::distance(it, end);
			const auto ptr = String::data() + pos;
			return basic_string_type(ptr, size);
		}

		auto sub(iterator it)
		{
			const auto end = String::end();
			return sub(it, end);
		}

		auto before(iterator it)
		{
			const auto begin = String::begin();
			return sub(begin, it);
		}

		auto after(iterator it)
		{
			const auto end = String::end();
			return sub(it == end ? end : std::next(it));
		}

		auto split(iterator it)
		{
			return std::pair { before(it), after(it) };
		}

		auto split(size_type pos)
		{
			const auto begin = String::begin();
			const auto end = String::end();
			const auto it = String::npos == pos ? end : std::next(begin, pos);
			return split(it);
		}

		using String::String;
		basic_string_type(const String& s)
		: String(s)
		{ }
	};

	template
	<
		class Char,
		template <class> class Traits = std::char_traits,
		template <class> class Alloc = std::allocator,
		template <class> class Order = std::less,
		class String = fwd::basic_string<Char, Traits, Alloc>,
		class View = fwd::basic_string_view<Char, Traits>,
		class Base = basic_string_type<View , Char, Traits, Alloc, Order>
	>
	struct basic_string_view : Base
	{
		struct order
		{
			using view = basic_string_view;
			constexpr bool operator()(view left, view right) const
			{
				return left.data() < right.data() and left.size() < right.size();
			}
		};

		using subset = std::set<Base, order, Alloc<Base>>;

		using Base::Base;
		basic_string_view(const String& in)
		: Base(in.data(), in.size())
		{ }
	};

	template
	<
		class Char,
		template <class> class Traits = std::char_traits,
		template <class> class Alloc = std::allocator,
		template <class> class Sort = std::less,
		class String = fwd::basic_string<Char, Traits, Alloc>,
		class View = fwd::basic_string_view<Char, Traits>,
		class Base = basic_string_type<String, Char, Traits, Alloc, Sort>
	>
	struct basic_string : Base
	{
		using view = basic_string_view<Char, Traits, Alloc, Sort>;

		using Base::Base;
		basic_string(const View& in)
		: Base(in.data(), in.size())
		{ }
	};

	// low 7 bit UTF-8
	using string = basic_string<char>;
	using view = string::view;
	// UTF-16 or UTF-32 wide
	using wstring = basic_string<wchar_t>;
	using wide = wstring::view;
	// at least a UTF-32 code point
	using ustring = basic_string<char32_t>;
	using code = ustring::view;
	// everything is a string view in UTF
	using buffer = view::buffer;
	using address = view::address;
	using iterator = view::iterator;
	using pointer = view::const_pointer;
	using vector = view::vector;
	using span = view::span;
	using subset = view::subset;
	using list = view::list;
	using map = view::map;
	using pair = view::pair;
	using set = view::set;
	using init = view::init;
	using binary = view::binary;
	using input = view::input;
	using output = view::output;
	using read = view::read;
	using write = view::write;
	using pipe = view::pipe;
	using test = view::test;
	using compare = view::compare;
	using cache = string::set;
	// parameters
	using table = layout<pair>;
	// position
	using size_type = view::size_type;
	using size = layout<size_type>;
	using size_pair = size::pair;
	// offset
	using diff_type = view::difference_type;
	using diff = layout<diff_type>;
	using diff_pair = diff::pair;

	constexpr auto npos = view::npos;
	constexpr size_type null = 0;

	template <class  Type>
	using as_view = fwd::as_view<Type>;
}

namespace fwd
{
	template <class Char> struct view_type<fmt::basic_string_view<Char>>
	{
		using type = fmt::basic_string_view<Char>;
	};

	template <class Char> struct view_type<fmt::basic_string<Char>>
	{
		using type = fmt::basic_string_view<Char>;
	};
}

namespace
{
	inline fmt::input operator>>(fmt::input in, fmt::read line)
	{
		return line(in);
	}

	inline fmt::output operator<<(fmt::output out, fmt::write line)
	{
		return line(out);
	}

	inline fmt::input operator>>(fmt::input in, fmt::pipe line)
	{
		return line.first(in);
	}

	inline fmt::output operator<<(fmt::output out, fmt::pipe line)
	{
		return line.second(out);
	}
}

#endif // file
