#ifndef fwd_hpp
#define fwd_hpp "Forward Declarations"

#include <iosfwd>
#include <utility>
#include <iterator>
#include <algorithm> 
#include <functional>
#include <type_traits>
#include <initializer_list>
#include <unordered_map>
#include <map>
#include <set>
#include <span>
#include <array>
#include <vector>
#include <list>
#include <tuple>
#include <string>
#include <sstream>
#include <string_view>
#include <locale>
#include <memory>

enum : bool { success = false, failure = true };

namespace fwd
{
	template <class Type> using as_ptr = typename std::add_pointer<Type>::type;
	template <class Type> using no_ptr = typename std::remove_pointer<Type>::type;
	template <class Type> using as_ref = typename std::add_lvalue_reference<Type>::type;
	template <class Type> using no_ref = typename std::remove_reference<Type>::type;
	
	template <class T, class S=T> using pair = std::pair<T, S>;
	template <class Type> using init = std::initializer_list<Type>;

	template
	<
		class Type, size_t Size = std::dynamic_extent
	>
	using span = std::span<Type, Size>;

	template
	<
		class Type, size_t Size
	>
	using array = std::array<Type, Size>;

	template
	<
		class Type, template <class> class Alloc = std::allocator
	>
	using vector = std::vector<Type, Alloc<Type>>;

	template
	<
		class Type, template <class> class Alloc = std::allocator
	>
	using list = std::list<Type, Alloc<Type>>;

	template
	<
		class Type, template <class> class Order = std::less, template <class> class Alloc = std::allocator
	>
	using set = std::set<Type, Order<Type>, Alloc<Type>>;

	template
	<
		class Key, class Value, template <class> class Order = std::less, template <class> class Alloc = std::allocator
	>
	using map = std::map<Key, Value, Order<Key>, Alloc<std::pair<const Key, Value>>>;

	template
	<
		class Char, template <class> class Traits = std::char_traits
	>
	using basic_string_view = std::basic_string_view<Char, Traits<Char>>;

	template
	<
		class Char, template <class> class Traits = std::char_traits, template <class> class Alloc = std::allocator
	>
	using basic_string = std::basic_string<Char, Traits<Char>, Alloc<Char>>;

	template
	<
		class Char, template <class> class Traits = std::char_traits
	>
	using basic_ios = std::basic_ios<Char, Traits<Char>>;

	template
	<
		class Char, template <class> class Traits = std::char_traits
	>
	using basic_istream = std::basic_istream<Char, Traits<Char>>;

	template
	<
		class Char, template <class> class Traits = std::char_traits
	>
	using basic_iostream = std::basic_iostream<Char, Traits<Char>>;

	template
	<
		class Char, template <class> class Traits = std::char_traits
	>
	using basic_ostream = std::basic_ostream<Char, Traits<Char>>;

	template
	<
		class Char, template <class> class Traits = std::char_traits
	>
	using basic_buf = std::basic_streambuf<Char, Traits<Char>>;

	template
	<
		class Char, template <class> class Traits = std::char_traits
	>
	using basic_file = std::basic_filebuf<Char, Traits<Char>>;

	template
	<
		class Char, template <class> class Traits = std::char_traits, template <class> class Alloc = std::allocator
	>
	using basic_stringstream = std::basic_stringstream<Char, Traits<Char>, Alloc<Char>>;

	//
	// Sub class with traits
	//
	
	template <class Type> struct type_of
	{
		using type = Type;
	};

	template
	<
		class Type, template <class...> class... Traits
	>
	struct class_of : Type, Traits<Type>...
	{
		using Type::Type;
	};

	//
	// View type for type
	//

	template
	<
		class Type, std::size_t Size = 0
	>
	struct view_type
	{
		using type = as_ref<const Type>;
	};

	template
	<
		class Type, std::size_t Size = 0
	>
	using as_view = typename view_type<Type, Size>::type;

	template
	<
		class Type, std::size_t Size
	>
	struct view_type<span<Type, Size>>
	{
		using type = span<Type, Size>;
	};

	template
	<
		class Type, std::size_t Size
	>
	struct view_type<Type[Size]>
	{
		using type = span<Type, Size>;
	};

	template
	<
		class Type, std::size_t Size
	>
	struct view_type<array<Type, Size>>
	{
		using type = span<Type, Size>;
	};

	template
	<
		class Char
	>
	struct view_type<basic_string_view<Char>>
	{
		using type = basic_string_view<Char>;
	};

	template
	<
		class Char
	>
	struct view_type<basic_string<Char>>
	{
		using type = basic_string_view<Char>;
	};
}

#endif // file
