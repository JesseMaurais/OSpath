#ifndef win_hpp
#define win_hpp "WIN32 Utility"

#ifndef _WIN32
# error WIN32 utility header included without feature macro.
#endif

#include <windows.h>
#include "ptr.hpp"

#ifdef WARN
# ifndef WINERROR
#  define WINERROR(n, ...) WARN(__VA_ARGS__, ::sys::win::strerr(n, nullptr)))
# endif
# ifndef WINERR
#  define WINERR(...) WINERROR(::GetLastError(), __VA_ARGS__)
# endif
#endif

namespace sys::win
{
	const char* strerr(DWORD, void*);

	inline auto invalid = INVALID_HANDLE_VALUE;

	inline bool fail(HANDLE h)
	{
		return nullptr == h or invalid == h;
	}

	inline HANDLE get(int fd)
	{
		if (sys::fail(fd)) return sys::win::invalid;
		auto const iptr = _get_osfhandle(fd);
		return reinterpret_cast<HANDLE>(iptr);
	}

	inline int open(HANDLE h, int flags)
	{
		if (sys::win::fail(h)) return sys::invalid;
		auto const iptr = reinterpret_cast<intptr_t>(h);
		return _open_osfhandle(iptr, flags);
	}

	inline DWORD wait(HANDLE h, DWORD ms = INFINITE)
	{
		const auto dw = WaitForSingleObject(h, ms);
		if (WAIT_FAILED == dw)
		{
			#ifdef WINERR
			WINERR("WaitForSingleObject", ms);
			#endif
		}
		return dw;
	}

	inline DWORD wait(span<HANDLE> h, DWORD ms = INFINITE, BOOL all = FALSE)
	{
		const auto dw = WaitForMultipleObjects((DWORD) h.size(), h.data(), all, ms);
		if (WAIT_FAILED == dw)
		{
			#ifdef WINERR
			WINERR("WaitForMultipleObjects", ms);
			#endif
		}
		return dw;
	}
	
	struct handle : fwd::shared_ptr<void>
	{	
		static auto close(HANDLE h)
		{
			return fwd::make_shared<void>(h, [](HANDLE h)
			{
				if (not fail(h) and not CloseHandle(h))
				{
					#ifdef WINERR
					WINERR("CloseHandle");
					#endif
				}
			});
		}
		
		handle(HANDLE h = invalid)
		: shared_ptr(enclose(h))
		{ }

		operator HANDLE() const
		{
			return get();
		}
	};
	
	struct local : fwd::shared_ptr<void>
	{	
		static auto close(HLOCAL h)
		{
			return fwd::make_shared<void>(h, [](HLOCAL h)
			{
				if (not fail(h) and (h = LocalFree(h)))
				{
					#ifdef WINERR
					WINERR("LocalFree");
					#endif
				}
			});
		}
		
		local(HLOCAL h = invalid)
		: shared_ptr(enclose(h))
		{ }
		
		operator HLOCAL() const
		{
			return get();
		}
	};
	
	template <class T> struct zero : T
	{
		static_assert(std::is_trivially_copyable<T>::value);
		static_assert(std::is_standard_layout<T>::value);

		zero()
		{
			ZeroMemory(this, sizeof(T));
		}

		operator fwd::as_ptr<const T>() const
		{
			return this;
		}
		
		operator fwd::as_ptr<T>()
		{
			return this;
		}
	};

	template
	<
		auto S, class T = fwd::offset_of<S>
	>
	struct size : zero<typename T::parent_type>
	{
		size()
		{
			constexpr auto s = sizeof(typename T::parent_type);
			this->*S = static_cast<typename T::value_type>(s);
		}
	};
	
	local sddl(LPSTR);
}

#endif // filehttps://www.youtube.com/
