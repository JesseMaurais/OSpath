#ifndef mode_hpp
#define mode_hpp "Access Mode"

#include "fmt.hpp"

namespace env::file
{
	using namespace fmt;
	
	int to_flags(view); // native open bits
	mode_t to_mode(view, mode_t = {}); // native mode bits
	string from_flags(int); // fopen style string
	string from_mode(mode_t, view = {}); // ls -l style string
}

#endif // file
