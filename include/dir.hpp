#ifndef dir_hpp
#define dir_hpp "File Directory"

#include "fmt.hpp"

namespace fmt::path
{
	vector split(view);
	string join(span);
	string join(init);

	template <class... View> auto join(view u, View... w)
	{
		return join({ u, w... });
	}
}

namespace fmt::dir
{
	vector split(view);
	string join(span);
	string join(init);

	template <class... View> auto join(view u, View... w)
	{
		return join({ u, w... });
	}
}

namespace fmt::file
{
	vector split(view);
	string join(span);
	string join(init);

	template <class... View> auto join(view u, View... w)
	{
		return join({ u, w... });
	}
}

namespace env::file
{
	using namespace fmt;

	bool any_of(view, test);
	bool any_of(span, test);
	bool any_of(init, test);

	template <class View> bool all_of(View it, test op)
	{
		return not any_of(it, not op);
	}

	test mask(view);
	test regex(view);
	test copy(string &);
	test copy(cache &);
}

#endif // file
