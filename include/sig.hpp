#ifndef sig_hpp
#define sig_hpp "Signals & Sockets"

#include "fmt.hpp"
#include "ptr.hpp"

namespace env::sig
{
	using view = fmt::view;
	using event = fwd::event;

	void set(view, event);
	event get(view);
	void raise(view);
	void all(event);
	
	void push(event);
	event pop();
	
	void def();
	void ign();
	void err();
}

#endif // file
