#ifndef fcntl_hpp
#define fcntl_hpp "POSIX File Control"

#include "uni.hpp"
#include "ptr.hpp"
#include <fcntl.h>

namespace sys::uni
{
	struct lock : fwd::zero<flock>
	{
		auto get(int fd)
		{
			return fcntl(fd, F_GETLK, this);
		}

		auto set(int fd)
		{
			return fcntl(fd, F_SETLK, this);
		}

		auto wait(int fd)
		{
			return fcntl(fd, F_SETLKW, this);
		}
	};
}

#endif // file
