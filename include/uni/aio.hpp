#ifndef aio_hpp
#define aio_hpp "POSIX Asyncrhonous I/O"

#include "uni.hpp"
#include "ptr.hpp"
#include <aio.h>

namespace sys::uni
{
	struct aio : fwd::zero<aiocb>
	{
		auto read()
		{
			return aio_read(this);
		}

		auto write()
		{
			return aio_write(this);
		}

		auto fsync(int op)
		{
			return aio_fsync(op, this);
		}

		auto error() const
		{
			return aio_error(this);
		}

		auto result()
		{
			return aio_return(this);
		}

		auto suspend(auto to=nullptr)
		{
			return aio_suspend(this, 1, to);
		}

		auto cancel(int fd)
		{
			return aio_cancel(fd, this);
		}
	};
}

#endif // file
