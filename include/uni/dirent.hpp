#ifndef dirent_hpp
#define dirent_hpp "POSIX Directory Entry"

#include <stdlib.h>
#include <dirent.h>

#include "uni.hpp"
#include "ptr.hpp"

namespace sys::uni
{
	struct dir : fwd::unique_ptr<DIR>
	{
		static auto enclose(DIR* that)
		{
			return fwd::make_unique<DIR>(that, [=](auto ptr)
			{
				#ifdef assert
				assert(that == ptr);
				#endif

				if (nullptr != ptr)
				{
					if (closedir(ptr))
					{
						perror("closedir");
					}
				}
			});
		}

		dir(const char *path)
		{
			if (auto ptr = opendir(path))
			{
				reset(ptr);
			}
			else
			{
				#ifdef perror
				perror("opendir", path);
				#else
				perror("opendir");
				#endif
			}
		}

		dir(int fd)
		{
			if (auto ptr = fdopendir(fd))
			{
				reset(ptr);
			}
			else
			{
				#ifdef perror
				perror("fdopendir", fd);
				#else
				perror("fdopendir");
				#endif
			}
		}

		dirent* next()
		{
			if (auto ptr = get())
			{
				return readdir(ptr);
			}
			else
			{
				return nullptr;
			}
		}
	};
}

namespace sys
{
	struct files : sys::uni::dir
	{
		using dir::dir;

		class iterator
		{
			files *that;
			dirent *ptr;

		public:

			iterator(files *dir, dirent *ent)
			: that(dir), ptr(ent)
			{ }

			bool operator!=(fwd::as_view<iterator> it) const
			{
				return (it.that != that) or (it.ptr != ptr);
			}

			auto operator*() const
			{
				return ptr->d_name;
			}

			auto operator++()
			{
				return ptr = that->next();
			}
		};

		auto begin()
		{
			return iterator(this, next());
		}

		auto end()
		{
			return iterator(this, nullptr);
		}
	};
}

#if __has_include(<sys/inotify.h>)
#define SYS_INOTIFY
#include <sys/inotify.h>
namespace sys::uni
{
	struct inotify
	{
		sys::uni::filed fd;

		inotify(int flags=0) : fd(inotify_init1(flags))
		{
			if (sys::fail(fd))
			{
				perror("inotify_init");
			}
		}

		auto add(const char* path, uint32_t mask)
		{
			int wd = inotify_add_watch(fd, path, mask);
			if (sys::fail(wd))
			{
				perror("inotify_add_watch");
			}
			return wd;
		}

		auto rm(int wd)
		{
			int no = inotify_rm_watch(fd, wd);
			if (sys::fail(no))
			{
				perror("inotify_rm_watch");
			}
			return no;
		}

		auto read(inotify_event* buf, size_t sz)
		{
			 ssize_t n = sys::read(fd, buf, sz);
			 if (sys::fail(n))
			 {
				perror("inotify_event");
			 }
			 return n;
		}
	};
}
#endif

#if __has_include(<sys/event.h>)
#define SYS_EVENT
#include <sys/event.h>
namespace sys::uni
{
	struct event : fwd::zero<kevent>
	{
		using zero::zero;
		event(uintptr_t id, auto fi=0, auto fl=0,  auto ffl=0, auto d=0, auto p=nullptr)
		{
			EV_SET(this, id, fi, fl, ffl, d, p);
		}
	};

	struct queue : fmt::layout<event>
	{
		filed fd;

		queue() : fd(kqueue())
		{
			if (fail(fd)) perror("kqueue");
		}

		ssize_t operator()(span in, span out, const timespec* to=nullptr) const
		{
			int nev = kevent(fd, in.data(), in.size(), out.data(), out.size(), to);
			if (fail(nev))
			{
				perror("kevent");
			}
			return nev
		}
	};
}
#endif

#endif // file
