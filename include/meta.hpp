#ifndef meta_hpp
#define meta_hpp "Document Templates"
#else // Do not include this header from another header!
#error Included more than once. Did you include it from a header? Do not!
#endif

#include "doc.hpp"
#include "sync.hpp"
#include "dig.hpp"
#include "algo.hpp"

#ifdef interface
#undef interface // struct in combaseapi.h
#endif

namespace doc
{
	// private

	template <class Type> struct element
	{
		std::vector<ptrdiff_t> index;
		std::vector<size_t> cross;
		std::vector<Type> item;

		element()
		{
			#ifdef assert
			static bool singleton = true;
			assert(singleton);
			singleton = false;
			#endif
		}
	};

	template <class Type> static sys::exclusive<element<Type>> local;
	extern sys::exclusive<std::unordered_map<std::type_index, fwd::as_ptr<interface>>> registry;

	// public

	template <class Type> instance<Type>::instance()
	{
		registry.writer()->emplace(type(), this);
	}

	template <class Type> fwd::as_ptr<instance<Type>> instance<Type>::address()
	{
		static instance singleton;
		return std::addressof(singleton);
	}

	template <class Type> const std::type_info& instance<Type>::type() const
	{
		return typeid(Type);
	}

	template <class Type> std::ptrdiff_t instance<Type>::size() const
	{
		return local<Type>.reader()->item.size();
	}

	template <class Type> typename fwd::shared_ptr<const Type> instance<Type>::reader(int n)
	{
		auto key = local<Type>.reader();
		auto ptr = key->item.data() + key->index.at(n);
		return fwd::shared_ptr<Type>(ptr, [key](auto ptr)
		{
			(void) key;
			#ifdef assert
			assert(nullptr != ptr);
			#endif
		});
	}

	template <class Type> typename fwd::shared_ptr<Type> instance<Type>::writer(int n)
	{
		auto key = local<Type>.writer();
		auto ptr = key->item.data() + key->index.at(n);
		return fwd::shared_ptr<Type>(ptr, [key](auto ptr)
		{
			(void) key;
			#ifdef assert
			assert(nullptr != ptr);
			#endif
		});
	}

	template <class Type> int instance<Type>::emplace(Type&& type)
	{
		auto key = local<Type>.writer();

		// find lowest free index
		auto pos = key->index.size();
		if (key->index.size() > key->item.size())
		{
			for (auto count : fwd::up_to(pos))
			{
				if (key->index.at(count) < 0)
				{
					pos = count;
					break;
				}
			}
		}
		// allocate a position
		auto const off = key->item.size();
		if (key->index.size() == pos)
		{
			key->index.push_back(off);
		}
		else
		{
			key->index.at(pos) = off;
		}
		// allocate an item
		key->item.emplace_back(std::move(type));
		key->cross.push_back(pos);

		#ifdef assert
		assert(key->cross.size() == key->item.size());
		assert(key->cross.at(key->index.at(pos)) == pos);
		#endif

		return fmt::to_int(pos);
	}

	template <class Type> void instance<Type>::destroy(int id) const
	{
		#ifdef assert
		assert(contains(id));
		#endif

		auto key = local<Type>.writer();

		const auto pos = fmt::to_size(id);
		const auto off = key->index.at(pos);
		key->item.at(off) = std::move(key->item.back());
		key->cross.at(off) = key->cross.back();
		key->index.at(key->cross.back()) = off;
		key->index.at(pos) = -1;
		while (key->index.back() < 0)
		{
			key->index.pop_back();
		}
		key->cross.pop_back();
		key->item.pop_back();

		#ifdef assert
		assert(key->cross.size() == key->item.size());
		assert(key->item.empty() or key->cross.at(key->index.at(off)) == fmt::to_size(off));
		assert(key->item.empty() or key->cross.at(key->index.at(off)) == fmt::to_size(off));
		#endif
	}

	template <class Type> bool instance<Type>::contains(int id) const
	{
		auto key = local<Type>.reader();

		if (auto pos = fmt::to_size(id); fmt::in_range(key->index, pos))
		{
			if (auto off = key->index.at(pos); fmt::in_range(key->item, off))
			{
				#ifdef assert
				assert(pos == key->cross.at(off));
				#endif
				return true;
			}
		}
		return false;
	}
}
