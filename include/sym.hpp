#ifndef sym_hpp
#define sym_hpp "Dynamic Link Library"

namespace sys
{
	struct lib
	{
		lib() = default;
		lib(const char* name);
		~lib();

		static lib find(const char* name);
		static lib& bin();

		template <class Type> auto sym(const char* name) const
		{
			Type *addr = nullptr;
			// see pubs.OpenGroup.org
			*(void**)(&addr) = sym(name);
			return addr;
		}

		operator bool() const
		{
			return nullptr != ptr;
		}

	private:

		void *ptr = nullptr;
		void *sym(const char* name) const;
	};

	template <class T> auto sym(const char* name)
	{
		return lib::bin().sym<T>(name);
	}
}

#endif // file