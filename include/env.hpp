#ifndef env_hpp
#define env_hpp "Environment Variables"

#include "fmt.hpp"

namespace env
{
	using namespace fmt;
	
	// Shell line expansion
	view echo(view);
	// Convert using program catalog
	view cat(view);
	// Accessors
	bool got(view);
	view get(view);
	bool set(view);
	bool put(view);
	// Common
	span path();
	view pwd();
	view home();
	view temp();
	view base();
	view root();
	view user();
	view host();
	view shell();
	view domain();
	view session();
}

#endif // file
