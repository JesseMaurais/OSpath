#ifndef win_com_hpp
#define win_com_hpp "Component Object Model"

#include <combaseapi.h>
#include <type_traits>
#include "ptr.hpp"

namespace sys::win
{
	template <class Type> inline const auto& uuid_of = __uuidof(Type);

	template <class Type> auto make_shared(Type* ptr)
	{
		static_assert(std::is_base_of<IUnknown, Type>::value);
		
		return fwd::make_shared(ptr, [=](Type* that)
		{
			#ifdef assert
			assert(that == ptr);
			assert(that != nullptr);
			#endif
			if (that)
			{
				that->Release();
			}
		});
	}
	
	template <class Type> auto query(IUnknown* ptr)
	{
		static_assert(std::is_base_of<IUnknown, Type>::value);
		
		if (Type* that; ptr->QueryInterface(uuid_of<Type>, &that) == S_OK)
		{
			return make_shared(that);
		}
		return null_shared<Type>();
	}
}

#endif // file
