#ifndef win_security_hpp
#define win_security_hpp "WIN32 Security Structures"

#include "win.hpp"
#include "ptr.hpp"

namespace sys::win
{
	struct attributes : size<&SECURITY_ATTRIBUTES::nLength>
	{	
		attr(PISECURITY_DESCRIPTOR lpsd = nullptr, bool inherit = true)
		{
			bInheritHandle = inherit ? TRUE : FALSE;
			lpSecurityDescriptor = lpsd;
		}
 
		auto thread(unsigned size, unsigned (*start)(void*), void* arg, unsigned flag, unsigned* addr)
		{
			return _beginthreadex(this, size, start, arg, flag, addr); // for the CRT
		}

		auto pipe(HANDLE h[2], DWORD sz = 0)
		{
			return CreatePipe(h + 0, h + 1, this, sz);
		}

		auto mutex(LPCSTR name = nullptr, bool owner = false)
		{
			return CreateMutex(this, owner, name);
		}

		auto semaphore(LPCSTR name = nullptr, long init = 0, long size = 8)
		{
			return CreateSemaphore(this, init, size, name);
		}

		auto event(LPCSTR name = nullptr, bool manual = false, bool init = false)
		{
			return CreateEvent(this, manual, init, name);
		}

		auto job(LPCSTR name = nullptr)
		{
			return CreateJobObject(this, name);
		}

		auto timer(LPCSTR name = nullptr, bool manual = false)
		{
			return CreateWaitableTimer(this, manual, name);
		}

		auto mapping(HANDLE h, LPCSTR name = nullptr, DWORD prot = 0, SIZE_T sz = 0)
		{
			return CreateFileMapping(h, this, prot, HIWORD(sz), LOWORD(sz), name);
		}
		
		auto process(LPSTR app, LPSTR cmd, LPSTR dir, LPVOID env, DWORD dw, LPSTARTUPINFO sa, LPPROCESS_INFORMATION pi)
		{
			return CreateProcess(app, cmd, this, this, bInheritHandle, dw, env, dir, sa, pi);
		}
	};
}

#endif // file