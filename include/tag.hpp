#ifndef tag_hpp
#define tag_hpp "String Constants"

#include "fmt.hpp"

namespace fmt::tag
{
	inline view empty = { };
	inline view nul = "\0";
	inline view eol = "\n";
	inline view tab = "\t";
	inline view dash = "-";
	inline view dual = "--";
	inline view dot = ".";
	inline view dots = "..";
	inline view space = " ";
	inline view slash = "/";
	inline view quote = "\"";
	inline view less = "<";
	inline view equal = "=";
	inline view greater = ">";
	inline pair square { "[", "]" };
	inline pair angle { "<", ">" };
	inline pair curl { "{", "}" };

	view put(view); // add to cache
	input get(input); // populate cache
	output set(output); // copy cache
}

#endif // file