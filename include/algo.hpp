#ifndef algo_hpp
#define algo_hpp "Algorithms"

#include "fwd.hpp"
#include "tmp.hpp"

namespace fwd
{
	template <class Type> auto up_to(Type size, Type pos = 0, Type step = 1)
	{
		struct iterator
		{
			Type pos;
			Type const step;
			
			bool operator!=(as_view<iterator> it) const
			{
				return (it.pos != pos) or (it.step != step);
			}

			auto operator*() const
			{
				return pos;
			}

			auto operator++()
			{
				pos += step;
			}
		};
		#ifdef assert
		assert(0 == ((size - pos) % step));
		#endif
		return make_range<iterator>
		(
			{ pos, step }, { size, step }
		);
	}

	template <class Type> auto down_from(Type size, Type pos = 0, Type step = 1)
	{
		struct iterator
		{
			Type pos;
			Type const step;
			
			bool operator!=(as_view<iterator> it) const
			{
				return (it.pos != pos) or (it.step != step);
			}

			auto operator*() const
			{
				return pos - 1;
			}

			auto operator++()
			{
				pos -= step;
			}
		};
		#ifdef assert
		assert(0 == ((size - pos) % step));
		#endif
		return make_range<iterator>
		(
			{ size, step }, { pos, step }
		);
	}
	
	template
	<
		class Range, class Predicate
	>
	bool all_of(Range &&r, Predicate p)
	{
		return std::all_of(r.begin(), r.end(), p);
	}

	template
	<
		class Range, class Predicate
	>
	bool any_of(Range &&r, Predicate p)
	{
		return std::any_of(r.begin(), r.end(), p);
	}

	template
	<
		class Range, class Predicate
	>
	auto find_if(Range &&r, Predicate p)
	{
		return std::find_if(r.begin(), r.end(), p);
	}

	template
	<
		class Range, class Offset, class Predicate
	>
	auto find_if(Range &&r, Offset off, Predicate p)
	{
		return find_if(r, [=](auto const &obj)
		{
			return p(obj.*off);
		});
	}

	template
	<
		class Input, class Output
	>
	void copy(Input && in, Output & out)
	{
		auto it = std::back_inserter(out);
		std::copy(in.begin(), in.end(), it);
	}
}

#endif // file
