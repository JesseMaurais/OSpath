#ifndef usr_hpp
#define usr_hpp "User Directory"

#include "fmt.hpp"

namespace env::usr
{
	using namespace fmt;

	// Special folder directory
	view dir(view);
	// User folders config file
	span user_dirs();
	// Name of the desktop environment
	view current_desktop();
	// Main menu onfiguration file
	view applications_menu();
	// Directory for runtime files
	span runtime();
	// Directory for config files
	span config();
	// Directory for cachh
	span cache();
	// Directory for program data
	span data();

	inline auto desktop()
	{
		return dir("Desktop");
	}

	inline auto documents()
	{
		return dir("Documents");
	}

	inline auto download()
	{
		return dir("Downloads");
	}

	inline auto music()
	{
		return dir("Music");
	}

	inline auto pictures()
	{
		return dir("Pictures");
	}

	inline auto share()
	{
		return dir("Public");
	}

	inline auto templates()
	{
		return dir("Templates");
	}

	inline auto videos()
	{
		return dir("Videos");
	}
}

#endif // file
