#ifndef doc_hpp
#define doc_hpp "Document Object Model"

#include "fwd.hpp"
#include "fmt.hpp"
#include "tmp.hpp"
#include "ptr.hpp"
#include <typeinfo>
#include <typeindex>

#ifdef interface
#undef interface // struct in "combaseapi.h"
#endif

namespace doc
{
	struct interface : fwd::no_copy
	{
		virtual const std::type_info& type() const = 0;
		virtual std::ptrdiff_t size() const = 0;
		virtual bool contains(int n) const = 0;
		virtual void destroy(int n) const = 0;
	};

	template <class Type> class instance final : public interface
	{
		instance();

	public:

		static fwd::as_ptr<instance> address();

		const std::type_info& type() const override;
		std::ptrdiff_t size() const override;
		bool contains(int n) const override;
		void destroy(int n) const override;

		fwd::shared_ptr<const Type> reader(int n);
		fwd::shared_ptr<Type> writer(int n);
		int emplace(Type &&);
	};

	// Function registry

	extern template class instance<fwd::event>;
	inline auto event = instance<fwd::event>::address();

	inline auto signal(fwd::event f)
	{
		return event->emplace(std::move(f));
	}

	inline auto raise(int n)
	{
		return event->reader(n)->operator()();
	}

	inline auto cancel(int n)
	{
		return event->destroy(n);
	}

	// Access fields

	template <std::size_t N, class C> constexpr auto index(const C* that)
	{
		return std::get<N>(that->table); // the N'th member of C's table
	}

	template <std::size_t N, class C> inline auto& get(const C* that)
	{
		return that->*index<N>(that); // read from the N'th member of C
	}

	template <std::size_t N, class C> inline auto& set(C* that)
	{
		return that->*index<N>(that); // write to the N'th member of C
	}
	
	// Name of an accessible field in table
	
	template <auto M> extern std::string_view name;

	template <std::size_t N, class C> constexpr auto key([[maybe_unused]] const C* that)
	{
		return name<index<N, C>(nullptr)>;
	}
	
	// Loop over members of a class

	template
	<
		std::size_t N = 0, class C, typename... T
	>
	typename std::enable_if_t<N == sizeof...(T)> for_each(const C* that, const std::tuple<T...>& table, auto action)
	{
		(void) that, (void) table, (void) action;
	}
	
	template
	<
		std::size_t N = 0, class C, typename... T
	>
	typename std::enable_if_t<N != sizeof...(T)> for_each(const C* that, const std::tuple<T...>& table, auto action)
	{
		action.template operator()<N>();
		for_each<N + 1>(that, table, action);
	}
	
	template <class C> void for_each(const C* that, auto action)
	{
		for_each(that, that->table, action);
	}
}

#endif // file
