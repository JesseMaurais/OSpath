#ifndef err_hpp
#define err_hpp "Error Format"
#else // Do not include this header from another header!
#error Included more than once. Did you include it from a header? Do not!
#endif

#include "sys.hpp"
#include <ostream>

#ifdef assert
#	undef assert
#	warning You should not include assert. Include __FILE__ instead.
#else
#	include <cassert>
#	undef assert
#endif

namespace sys
{
	// thread local buffer
	std::ostream& out();
	// flush local buffer into stream
	std::ostream& put(std::ostream&);

	struct where
	{
		const char * file;
		const char * func;
		const size_t line;
	};

	template <class... Type>
	auto& print(std::ostream& out, const where& at, const Type&... args)
	{
		out << at.file << "(" << at.line << ")" << at.func << ":";
		if constexpr (0 < sizeof...(Type)) ((out << ' ' << args), ...);
		return out;
	}

	template <class... Type>
	auto& warn(const where& at, const Type&... args)
	{
		return print(out(), at, args...);
	}

	template <class... Type>
	auto& err(int n, const where& at, const Type&... args)
	{
		return warn(at, strerr(n), args...);
	}
}

#ifndef NDEBUG
#	define HERE sys::where { __FILE__, __func__, __LINE__ }
#	define ERR(n, ...) sys::err(n, HERE, __VA_ARGS__)
#	define WARN(...) sys::warn(HERE, __VA_ARGS__)
#	define ASSERT(X) if (not(X)) WARN(#X)
#	define EXCEPT(X) try { (void)(X); WARN(#X); } catch (...) { }
#	define TEST(X) dynamic void X##_UNIT_TEST_SUFFIX ()
#endif

namespace sys::test
{
	constexpr auto suffix = "_UNIT_TEST_SUFFIX";
};

#ifdef ASSERT
#	define assert(...) ASSERT(__VA_ARGS__)
#endif

#ifdef ERR
#	define perror(...) ERR(errno, __VA_ARGS__)
#endif
