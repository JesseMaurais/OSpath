#ifndef uid_hpp
#define uid_hpp "Universally Unique Id"

#include "fmt.hpp"

namespace fmt
{
	using uuid = view;
	using guid = uuid;
	
	inline uuid nullid = "{00000000-0000-0000-0000-000000000000}";
}

namespace doc
{
	template <class> extern fmt::uuid uuid_of;
}

#endif // file
